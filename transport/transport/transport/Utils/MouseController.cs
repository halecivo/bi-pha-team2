﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace transport.Utils
{
    enum MouseButton
    {
        Right,
        Middle,
        Left

    }

    static class MouseController
    {
        private static MouseState previousMouseState;
        private static MouseState currentMouseState;
        private static GraphicsDevice gdevice;

        public static bool Click(MouseButton button)
        {
            bool previousPress, currentPress;
            switch (button)
            {
                case MouseButton.Left:
                    previousPress = (previousMouseState.LeftButton == ButtonState.Pressed);
                    currentPress = (currentMouseState.LeftButton == ButtonState.Pressed);
                    break;
                case MouseButton.Middle:
                    previousPress = (previousMouseState.MiddleButton == ButtonState.Pressed);
                    currentPress = (currentMouseState.MiddleButton == ButtonState.Pressed);
                    break;

                case MouseButton.Right:
                    previousPress = (previousMouseState.RightButton == ButtonState.Pressed);
                    currentPress = (currentMouseState.RightButton == ButtonState.Pressed);
                    break;
                default:
                    currentPress = previousPress = false;
                    break;
            }

            return (!previousPress && currentPress);
        }

        public static bool Pressed(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.Left:
                    return currentMouseState.LeftButton == ButtonState.Pressed;
                case MouseButton.Middle:
                    return currentMouseState.MiddleButton == ButtonState.Pressed;
                case MouseButton.Right:
                    return currentMouseState.RightButton == ButtonState.Pressed;
                default:
                    return false;
            }
        }

        public static void Update(MouseState newState)
        {
            previousMouseState = currentMouseState;
            currentMouseState = newState;
        }

        public static void SetGDevice(GraphicsDevice device)
        {
            gdevice = device;
        }

        /// <summary>
        /// Spočítá souřadnici v terénu, kam bylo kliknuto.
        /// </summary>
        /// <param name="camera"></param>
        /// <returns></returns>
        public static Vector3 Position3D(ref Camera camera)
        {
            Vector3 from = camera.position;
            Vector3 to = gdevice.Viewport.Unproject(
                new Vector3(currentMouseState.X, currentMouseState.Y, 0),
                camera.projectionMatrix,
                camera.viewMatrix,
                Matrix.Identity
                );
            Vector3 direction = to - from;
            Ray pickRay = new Ray(from, direction);

            Vector3 result = pickRay.Position + (pickRay.Direction / Math.Abs(pickRay.Direction.Z)) * pickRay.Position.Z;
            return result;
        }
    }
}
