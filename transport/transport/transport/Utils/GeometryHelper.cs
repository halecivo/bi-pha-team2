﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace transport
{
    class GeometryHelper
    {
        /// <summary>
        /// Provede rotaci bodu A kolem počátku.
        /// </summary>
        /// <param name="point">rotovaný bod A</param>
        /// <param name="originPoint">počátek</param>
        /// <param name="rotationAxis">rotační osa</param>
        /// <param name="radiansToRotate">úhel rotace (radiány)</param>
        /// <returns>bod A po rotaci</returns>
        public static Vector3 RotateAroundPoint(Vector3 point, Vector3 originPoint, Vector3 rotationAxis, float radiansToRotate)
        {
            radiansToRotate %= MathHelper.TwoPi;
            
            Vector3 diffVect = point - originPoint;

            Vector3 rotatedVect = Vector3.Transform(diffVect, Microsoft.Xna.Framework.Matrix.CreateFromAxisAngle(rotationAxis, radiansToRotate));

            rotatedVect += originPoint;

            return rotatedVect;
        }

        /// <summary>
        /// Neorientovaný úhel mezi dvěma vektory.
        /// </summary>
        /// <param name="v1">vektor 1</param>
        /// <param name="v2">vektor 2</param>
        /// <returns>neorientovaný úhel</returns>
        public static float UnsignedAngleBetweenTwoV3(Vector3 v1, Vector3 v2)
        {
            v1.Normalize();
            v2.Normalize();
            double Angle = (float)Math.Acos(Vector3.Dot(v1, v2));
            return (float)Angle;
        }

        /// Find the angle between two vectors. This will not only give the angle difference, but the direction.
        /// For example, it may give you -1 radian, or 1 radian, depending on the direction. Angle given will be the 
        /// angle from the FromVector to the DestVector, in radians.
        /// </summary>
        /// <param name="FromVector">Vector to start at.</param>
        /// <param name="DestVector">Destination vector.</param>
        /// <param name="DestVectorsRight">Right vector of the destination vector</param>
        /// <returns>Signed angle, in radians</returns>        
        /// <remarks>All three vectors must lie along the same plane.</remarks>
        public static double SignedAngleBetween2DVectors(Vector3 FromVector, Vector3 DestVector, Vector3 DestVectorsRight)
        {
            FromVector.Normalize();
            DestVector.Normalize();
            DestVectorsRight.Normalize();

            float forwardDot = Vector3.Dot(FromVector, DestVector);
            float rightDot = Vector3.Dot(FromVector, DestVectorsRight);

            // Keep dot in range to prevent rounding errors
            forwardDot = MathHelper.Clamp(forwardDot, -1.0f, 1.0f);

            double angleBetween = Math.Acos(forwardDot);

            if (rightDot < 0.0f)
                angleBetween *= -1.0f;

            return angleBetween;
        }

        /// <summary>
        /// Nalezne střed kružnice opsané bodům A, B, C.
        /// </summary>
        /// <param name="A">bod A</param>
        /// <param name="B">bod B</param>
        /// <param name="C">bod C</param>
        /// <returns>střed kružnice</returns>
        public static Vector3 FindS(Vector3 A, Vector3 B, Vector3 C)
        {
            Vector3 OAB = (A + B) / 2;
            Vector3 OAC = (A + C) / 2;
           
            Vector2 P = new Vector2(
                (B.X - A.X) * OAB.X + (B.Y - A.Y) * OAB.Y,
                (C.X - A.X) * OAC.X + (C.Y - A.Y) * OAC.Y
                );

            float mx = (B.X - A.X) * (C.Y - A.Y) + (B.Y - A.Y) * (A.X - C.X);

            Vector3 S = new Vector3(
                (P.X * (C.Y - A.Y) + P.Y * (A.Y - B.Y)) / mx,
                (P.X * (A.X - C.X) + P.Y * (B.X - A.X)) / mx,
                (A.Z + B.Z + C.Z) / 3
                );


            return S;
        }

        /// <summary>
        /// Vytvoří kolmý vektor.
        /// </summary>
        /// <param name="v">původní vektor</param>
        /// <param name="rotationAxis">kolmice původního a výsledného vektoru</param>
        /// <returns></returns>
        public static Vector3 VectorPerpendicular(Vector3 v, Vector3 rotationAxis){
            return RotateAroundPoint(v, Vector3.Zero, rotationAxis, MathHelper.Pi / 2);
        }

        /// <summary>
        /// Nalezne průsečík dvou přímek.
        /// </summary>
        /// <param name="A">bod přímky 1</param>
        /// <param name="t">směrový vektor přímky 1</param>
        /// <param name="B">bod přímky 2</param>
        /// <param name="u">směrový vektor přímky 2</param>
        /// <returns>průsečík</returns>
        public static Vector3 LineIntersection(Vector3 A, Vector3 t, Vector3 B, Vector3 u)
        {
            if ((u.Y * t.X - u.X * t.Y) == 0)
            {
                throw new ArithmeticException("Nelze najít průsečík.");
            }

            float m = (u.X * (A.Y - B.Y) - u.Y * (A.X - B.X))
                      / (u.Y * t.X - u.X * t.Y);

            Vector3 P = A + m * t;

            return P;
        }

        public static bool LineIntersection(Vector3 A, Vector3 t, Vector3 B, Vector3 u, ref Vector3 P)
        {
            if ((u.Y * t.X - u.X * t.Y) == 0)
            {
                return false;
            }

            float m = (u.X * (A.Y - B.Y) - u.Y * (A.X - B.X))
                      / (u.Y * t.X - u.X * t.Y);

            P = A + m * t;

            return true;
        }

        public static float isOnLine(Vector3 A, Vector3 t, Vector3 B)
        {
            Vector3 smer = B - A;

            float test = Vector3.Cross(smer, t).Z;

            if (test != 0)
            {
                Console.WriteLine("Vektor je " + test); 
                return 0;
            }

            return smer.Length();
        }

        /// <summary>
        /// Převede Vector3 na Vector2.
        /// </summary>
        /// <param name="v">vektor3</param>
        /// <returns>vektor2</returns>
        public static Vector2 Vector3ToVector2(Vector3 v)
        {
            return new Vector2(v.X, v.Y);
        }

        /// <summary>
        /// Převede Vector2 na Vector3.
        /// </summary>
        /// <param name="v">vektor2</param>
        /// <param name="Z">Z souřadnice výsledného vektoru</param>
        /// <returns></returns>
        public static Vector3 Vector2ToVector3(Vector2 v, float Z = 0)
        {
            return new Vector3(v.X, v.Y, Z);
        }

        public static Vector3 VectorZ(Vector3 v, float Z = 0)
        {
            return new Vector3(v.X, v.Y, Z);
        }

        public static bool TouchCircle(Vector3 A, Vector3 Atan, Vector3 S, float r, ref Vector3 P){
            Vector3 Aperp = GeometryHelper.VectorPerpendicular(Atan, new Vector3(0, 0, 1));
            Vector3 AT1 = A + Vector3.Normalize(Aperp) * r;
            Vector3 AT2 = A - Vector3.Normalize(Aperp) * r;

            float distance = isOnLine(AT1, Atan, S);
            if (distance != 0)
            {
                P = A + Atan * distance;
                return true;
            }

            distance = isOnLine(AT2, Atan, S);
            if (distance != 0)
            {
                P = A + Atan * distance;
                return true;
            }
            Console.WriteLine("Body {0} = {4}, {1} = {5} jsou vzdáleny {2} od bodu {3}", AT1, AT2, r, A, (AT1 - A).Length(), (AT2 - A).Length());
            return false;
        }
    }
}
