﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace transport.Objects
{
    public enum BuildingType
    {
        Standard
    }

    class BuildingKatalog
    {
        List<Building> budovy = new List<Building>();
        Dictionary<BuildingType, Model> models = new Dictionary<BuildingType,Model>();
        GraphicsDevice gdevice;
        float defaultScale;

        public BuildingKatalog(GraphicsDevice gdevice, float defaultScale = 1.0f)
        {
            this.gdevice = gdevice;
            this.defaultScale = defaultScale;
        }

        public void AddModel(BuildingType type, Model model)
        {
            models.Add(type, model);
        }

        public void Add(Vector3 position, float rotation = 0, BuildingType type = BuildingType.Standard, float scale = 3.0f)
        {
            scale *= defaultScale;
            budovy.Add(
                new Building(position, models[type], Matrix.Identity, gdevice, scale, rotation)
                );
        }

        public void Draw(Effect shader, RTarget RT)
        {
            foreach(Building budova in budovy){
                budova.Draw(shader, RT);
            }
        }
    }
}
