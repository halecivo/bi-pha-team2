﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace transport
{
    /// <summary>
    /// Nadřazená třída všem silničním částem.
    /// </summary>
    abstract class Silnice
    {
        public bool deletable = true;
        public bool clicked = false;
        public bool built = true;

        public abstract void Draw(Effect effect);
        public abstract KonecSilnice getKonec(Vector3 x);
        public abstract List<KonecSilnice> getAllEnds ();
        public abstract void setEdge (Edge e);
        public abstract int getID();
        public abstract Vector3 getVectorID();
        public abstract void setID(IDStruct id);

        public abstract float getLength();
        public abstract float getLength(Vector3 smer);
        public abstract float getPassTime(Vector3 smer);
        public abstract int getMaxSpeed(Vector3 smer);
        public abstract int getPrice();
        public abstract int getDestroyPrice();
    }
}
