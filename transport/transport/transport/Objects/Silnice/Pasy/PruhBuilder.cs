﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace transport
{
    class PruhBuilder
    {
        Pruh pruh;
        Vector3 A, B, S;
        int smer;

        public PruhBuilder(Pruh pruh, Vector3 A, Vector3 B, Vector3 S, int smer = 1)
        {
            this.pruh = pruh;
            this.A = A;
            this.B = B;
            this.S = S;
            this.smer = smer;
        }

        public PruhBuilder(Pruh pruh, Vector3 A, Vector3 B, int smer = 1)
        {
            this.pruh = pruh;
            this.A = A;
            this.B = B;
            this.S = A;
            this.smer = smer;
        }


        public PruhVOblouku Create(GraphicsDevice gdevice)
        {
            if (S == A)
                return new PruhVOblouku(pruh, A, B, ref gdevice);
            else
                return new PruhVOblouku(pruh, A, B, S, ref gdevice);
        }
    }
}
