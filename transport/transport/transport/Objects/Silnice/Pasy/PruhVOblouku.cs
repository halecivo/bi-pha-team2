﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using transport.Utils;

namespace transport
{

    class PruhVOblouku
    {
        Pruh pruh;
        GeometricBuffer<VertexPositionNormalTexture> buffer;
        static int testDelicka = 1;

        KonecSilnice A, B;
        Vector3 S;

        public int MaxSpeed;
        public float Length = 0;

        public float getPassTime()
        {
            return Length / (MaxSpeed* 3.6f);
        }

        public KonecSilnice getA()
        {
            return A;
        }

        public KonecSilnice getB()
        {
            return B;
        }

        public void addDilek(Vector3 A, Vector3 B, Vector3 S, ref float offset, ref VertexPositionNormalTextureGeometricBufferFactory factory)
        {

            Vector3 normalA = Vector3.Normalize(S - A) * pruh.getSize() / 2;
            Vector3 A1 = A + normalA;
            Vector3 A2 = A - normalA;

            Vector3 normalB = Vector3.Normalize(S - B) * pruh.getSize() / 2;
            Vector3 B1 = B + normalB;
            Vector3 B2 = B - normalB;

            float len1 = (B1 - A1).Length();
            float len2 = (B1 - A1).Length();

            factory.AddTriangle(
                    B2, new Vector2(pruh.getTextureStart().X, len2 / 8 + offset),
                    A2, new Vector2(pruh.getTextureStart().X, offset),
                    A1, new Vector2(pruh.getTextureStart().X + pruh.getSize() / 8, offset));

            factory.AddTriangle(
                    B1, new Vector2(pruh.getTextureStart().X + pruh.getSize() / 8, len1 / 8 + offset),
                    A1, new Vector2(pruh.getTextureStart().X + pruh.getSize() / 8, offset),
                    B2, new Vector2(pruh.getTextureStart().X, len2 / 8 + offset));

            offset += (len1 + len2) / 16;
            Length += (A - B).Length();
        }

        public void addDilek(Vector3 A1, Vector3 A2, Vector3 B1, Vector3 B2, ref float offset, ref VertexPositionNormalTextureGeometricBufferFactory factory)
        {
            float len = (B1 - A1).Length();


            factory.AddTriangle(
                    B2, new Vector2(pruh.getTextureStart().X, len / 8 + offset),
                    A2, new Vector2(pruh.getTextureStart().X, offset),
                    A1, new Vector2(pruh.getTextureStart().X + pruh.getSize() / 8 - 1 / 8, offset));

            factory.AddTriangle(
                    B1, new Vector2(pruh.getTextureStart().X + pruh.getSize() / 8 - 1 / 8, len / 8 + offset),
                    A1, new Vector2(pruh.getTextureStart().X + pruh.getSize() / 8 - 1 / 8, offset),
                    B2, new Vector2(pruh.getTextureStart().X, len / 8 + offset));

            offset += len / 8;
            Length += ( (B1 - A1).Length() + (B2 - A2).Length()) / 2;
        }

        /// <summary>
        /// Zahnutý oblouk. Pozor! Vytvoří správně jen oblouky kratší než PI.
        /// </summary>
        /// <param name="pruh"></param>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="S"></param>
        /// <param name="gdevice"></param>
        public PruhVOblouku(Pruh pruh, Vector3 A, Vector3 B, Vector3 S, ref GraphicsDevice gdevice)
        {
            this.pruh = pruh;

            // vygenerování úseků
            Vector3 rA = A - S;
            Vector3 rB = B - S;


            float angle = GeometryHelper.UnsignedAngleBetweenTwoV3(rA, rB);
            int pocetUseku = (int)Math.Round((double)(angle / Math.PI * 180)) / testDelicka;
            VertexPositionNormalTextureGeometricBufferFactory factory = new VertexPositionNormalTextureGeometricBufferFactory();

            Vector3 P = A;
            Vector3 R;
            Vector3 rotAxis = Vector3.Normalize(Vector3.Cross(A - S, B - S));
            float offset = 0;
            for (int i = 0; i < pocetUseku; i++)
            {
                R = GeometryHelper.RotateAroundPoint(P, S, rotAxis, angle / pocetUseku);
                this.addDilek(P, R, S, ref offset, ref factory);
                P = R;
            }

            buffer = factory.Create(gdevice);

            this.S = S;
            this.A = new KonecSilnice(A, rA);
            this.B = new KonecSilnice(B, rB);
            MaxSpeed = GameLogic.GetMaxSpeed(rA.Length());
        }

        /// <summary>
        /// Zahnutý oblouk.
        /// </summary>
        /// <param name="pruh"></param>
        /// <param name="A">Počáteční bod.</param>
        /// <param name="B">Koncový bod.</param>
        /// <param name="BtoA">Tečna v koncovém bodě.</param>
        /// <param name="S">Střed oblouku.</param>
        /// <param name="gdevice"></param>
        public PruhVOblouku(Pruh pruh, Vector3 A, Vector3 B, Vector3 BtoA, Vector3 S, ref GraphicsDevice gdevice)
        {
            this.pruh = pruh;


            // vygenerování úseků
            Vector3 rA = A - S;
            Vector3 rB = B - S;



            Vector3 rotAxis = new Vector3(0, 0, 1);

            Vector3 rightVector = GeometryHelper.VectorPerpendicular(rB, -Vector3.Normalize(Vector3.Cross(A - B, BtoA)));

            float angle = (float)GeometryHelper.SignedAngleBetween2DVectors(Vector3.Normalize(rA), Vector3.Normalize(rB), rightVector);
            if (angle == MathHelper.Pi)
            {
                rotAxis = new Vector3(0, 0, 1);
                rotAxis = -Vector3.Normalize(Vector3.Cross(B - S, BtoA));
            }
            else if (angle == -1 * MathHelper.Pi)
            {
                rotAxis = new Vector3(0, 0, -1);
                rotAxis = -Vector3.Normalize(Vector3.Cross(B - S, BtoA));
                angle += MathHelper.TwoPi;
            }
            else if (angle < 0)
            {
                angle += MathHelper.TwoPi;
                rotAxis = Vector3.Normalize(Vector3.Cross(B - S, A - S));
                rotAxis = -Vector3.Normalize(Vector3.Cross(B - S, BtoA));
            }
            else
            {
                rotAxis = Vector3.Normalize(Vector3.Cross(A - S, B - S));
                rotAxis = -Vector3.Normalize(Vector3.Cross(B - S, BtoA));
            }
            int pocetUseku = (int)Math.Round(MathHelper.ToDegrees(angle)) / testDelicka;

            
            VertexPositionNormalTextureGeometricBufferFactory factory = new VertexPositionNormalTextureGeometricBufferFactory();


            Vector3 P = A;
            Vector3 R;
            float offset = 0;
            for (int i = 0; i < pocetUseku; i++)
            {
                
                R = GeometryHelper.RotateAroundPoint(P, S, rotAxis, angle / pocetUseku);
                if (float.IsNaN(R.X))
                {
                    throw new ArgumentException("Rotace bodu " + R + " kolem osy " + rotAxis + " o úhel " + MathHelper.ToDegrees(angle) +  " se nezdařila.");
                }
                this.addDilek(P, R, S, ref offset, ref factory);
                P = R;
            }

            buffer = factory.Create(gdevice);

            this.S = S;
            this.A = new KonecSilnice(A, rA);
            this.B = new KonecSilnice(B, rB);

            MaxSpeed = GameLogic.GetMaxSpeed(rA.Length());

        }


        // rovný oblouk
        public PruhVOblouku(Pruh pruh, Vector3 A, Vector3 B, ref GraphicsDevice gdevice, int smer = 1)
        {
            this.pruh = pruh;

            Vector3 nAB = Vector3.Normalize(Vector3.Cross(B - A, Vector3.Backward)) * this.pruh.getSize() / 2;

            float offset = 0;
            VertexPositionNormalTextureGeometricBufferFactory factory = new VertexPositionNormalTextureGeometricBufferFactory();
            this.addDilek(A + nAB, A - nAB, B + nAB, B - nAB, ref offset, ref factory);

            buffer = factory.Create(gdevice);

            this.S = A;
            this.A = new KonecSilnice(A, nAB);
            this.B = new KonecSilnice(B, nAB);

            MaxSpeed = GameLogic.GetMaxSpeed(1000);

        }

        public PruhVOblouku(Pruh pruh, KonecSilnice A, KonecSilnice B, float Poffset, ref GraphicsDevice gdevice)
        {
            this.pruh = pruh;

            
            Vector3 nA = Vector3.Normalize(A.getNormal());
            Vector3 nB = -Vector3.Normalize(B.getNormal());

            Vector3 Ap = A.getPosition() + Poffset*nA;
            Vector3 Bp = B.getPosition() + Poffset*nB;

            nA *= pruh.getSize() / 2;
            nB *= pruh.getSize() / 2;

            float offset = 0;
            VertexPositionNormalTextureGeometricBufferFactory factory = new VertexPositionNormalTextureGeometricBufferFactory();
            this.addDilek(Ap + nA, Ap - nA, Bp + nB, Bp - nB, ref offset, ref factory);

            buffer = factory.Create(gdevice);

            this.S = Ap;
            this.A = A;
            this.B = B;

            // TODO MAxspeed
            MaxSpeed = GameLogic.GetMaxSpeed(100);
        }

        public Pruh getPruh()
        {
            return pruh;
        }

        public void Draw(Effect effect)
        {
            buffer.Draw(effect);
        }




    }

}
