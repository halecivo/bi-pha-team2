﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;


namespace transport
{
    static class PruhType{

        public static Pruh A275, A300, A325, A350, A375,
                C200, C250, C300,
                E050, E075,
                PLNA, PRERUSOVANA, PRAZDNA;
        
        private static int MERITKO = 32; // kolik px textury odpovídá 25 cm

        static PruhType(){
            float TOffset = 0;// (float)1 / 64;

            A275 = new Pruh(2.75f, new Vector2(0, 0));
            A300 = new Pruh(3.00f, new Vector2(TOffset, 0));
            A325 = new Pruh(3.25f, new Vector2(0, 0));
            A350 = new Pruh(3.50f, new Vector2(0, 0));
            A375 = new Pruh(3.75f, new Vector2(0, 0));

            C200 = new Pruh(2.00f, new Vector2(0, 0));
            C250 = new Pruh(2.50f, new Vector2(0, 0));
            C300 = new Pruh(3.00f, new Vector2(0, 0));

            E050 = new Pruh(0.50f, new Vector2(0.5625f, 0));
            E075 = new Pruh(0.75f, new Vector2(0.5625f, 0));

            PLNA = new Pruh(0.25f, new Vector2(0.5f, 0));
            PRERUSOVANA = new Pruh(0.25f, new Vector2(0.53125f, 0));
            PRAZDNA = new Pruh(0.25f, new Vector2(TOffset, 0));
        }

    }
}
