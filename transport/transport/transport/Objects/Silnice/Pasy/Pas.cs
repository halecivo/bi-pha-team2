﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace transport
{
    class Pas
    {
        private List<PruhVOblouku> pruhy = new List<PruhVOblouku>();
        private float size = 0;

        public Pas(List<PruhVOblouku> pruhy)
        {
            this.pruhy = pruhy;
        }

        public List<PruhVOblouku> getPruhy()
        {
            return this.pruhy;
        }

        public float getSize()
        {
            return this.size;
        }
    }
}
