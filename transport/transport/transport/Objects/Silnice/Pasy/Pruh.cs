﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace transport
{
    class Pruh
    {

        private float size;
        private Vector2 textureStart;

        //public Pruh(string type)
        public Pruh(float size, Vector2 textureStart)
        {
            // vytahne sirku z ini souboru
            //this.sirka = Config.ToSingle(config[LanesSize][type])
            this.size = size;
            this.textureStart = textureStart;
        }

        public float getSize()
        {
            return this.size;
        }

        public Vector2 getTextureStart()
        {
            return this.textureStart;
        }
    }
}
