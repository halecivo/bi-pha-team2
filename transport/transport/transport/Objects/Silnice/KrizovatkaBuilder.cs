﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace transport
{
    class KrizovatkaBuilder
    {
        List<PruhBuilder> pruhy = new List<PruhBuilder>();
        List<KonecSilnice> konce = new List<KonecSilnice>();

        public void Add(PruhBuilder pruh)
        {
            pruhy.Add(pruh);
        }

        public void AddKonec(KonecSilnice konec)
        {
            konce.Add(konec);
        }

        public Krizovatka Create(GraphicsDevice gdevice){
            List<PruhVOblouku> obloucky = new List<PruhVOblouku>();
            foreach(PruhBuilder pruh in pruhy){
                obloucky.Add (pruh.Create(gdevice));
            }
            return new Krizovatka(obloucky, konce);
        }
    }
}
