﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace transport
{
    
    class Krizovatka : Silnice
    {
        List<PruhVOblouku> pruhy;
        public List<KonecSilnice> konce;
        public Edge edge = null;
        List<Pas> pravePasy, levePasy;

        private IDStruct idStruct;

        public override float getPassTime(Vector3 smer)
        {
            return getLength(smer) / (getMaxSpeed(smer) * 3.6f);
        }

        public override int getMaxSpeed(Vector3 smer)
        {
            return 40;
        }

        public override float getLength(Vector3 smer)
        {
            return 30;
        }

        public override float getLength()
        {
            return 30;
        }

        public override void setEdge(Edge e)
        {
            // v ramci krizovatky maji cesty stejne parametry, takze staci nastavit jen jednou
            if (this.edge == null)
                this.edge = e;
        }

        public override void setID(IDStruct id)
        {
            this.idStruct = id;
        }

        public override int getID()
        {
            return this.idStruct.id;
        }

        public override Vector3 getVectorID()
        {
            return this.idStruct.idVector;
        }

        public override int getPrice()
        {
            return 20000;
        }

        public override int getDestroyPrice()
        {
            return getPrice() / 3;
        }

        public KonecSilnice getA(int i = 0){
            if (konce == null){
                return null;
            }
            if (i > (konce.Count - 1) || i < 0)
            {
                return null;
            }
            return konce[i];
        }

        /// <summary>
        /// Vrátí nejbližší konec silnice vzhledem k souřadnicím.
        /// </summary>
        /// <param name="x">Testovací souřadnice</param>
        /// <returns>Nejbližší</returns>
        public override KonecSilnice getKonec(Vector3 x)
        {
            int i = 0;
            return getKonec(x, ref i);
        }

        /// <summary>
        /// Vrátí nejbližší konec silnice vzhledem k souřadnicím.
        /// </summary>
        /// <param name="x">Testovací souřadnice</param>
        /// <param name="i">Návratový parametr s indexem konce.</param>
        /// <returns>Nejbližší</returns>
        public KonecSilnice getKonec(Vector3 x, ref int i)
        {
            KonecSilnice min = konce[0];
            i = 0;
            float minLength = (min.getPosition() - x).Length();
            for (int j = 1; j < konce.Count; j++)
            {
                if ((konce[j].getPosition() - x).Length() < minLength)
                {
                    min = konce[j];
                    minLength = (konce[j].getPosition() - x).Length();
                    i = j;
                }
            }
            return min;
        }

        public Pas getPravyPas(int i = 0)
        {
            if (pravePasy == null)
            {
                return null;
            }
            if (i > (pravePasy.Count - 1) || i < 0)
            {
                return null;
            }
            return pravePasy[i];
        }

        // vrati seznam vsech koncu silnice
        public override List<KonecSilnice> getAllEnds()
        {
            return this.konce;        
        }

        public Krizovatka(List<PruhVOblouku> pruhy, List<KonecSilnice> konce)
        {
            this.pruhy = pruhy;
            this.konce = konce;
            foreach (PruhVOblouku pruh in pruhy)
            {
                foreach (KonecSilnice konec in konce)
                {
                    if (Vector3.Normalize((pruh.getA().getPosition() - konec.getPosition())) == Vector3.Normalize(pruh.getA().getNormal()))
                    {
                        
                    }
                    
                }
            }
        }

        public override void Draw(Effect effect)
        {
            foreach (PruhVOblouku pruh in pruhy)
            {
                pruh.Draw(effect);
            }
        }
    }
}
