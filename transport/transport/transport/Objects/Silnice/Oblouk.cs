﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using transport.Utils;

namespace transport
{
    /**
     * Třída vykreslující jeden oblouk (i rovný) mezi body A, B (případně S pro nerovný). 
     * Oblouk bude vykreslen pomocí rovných úseků třídy Usek úhlu maximálně 1°.
     */
    class Oblouk : Silnice
    {
        private List<Pruh> pruhy = new List<Pruh>();

        private Vector3 S;

        public Pas pravyPas, stredovyPas, levyPas;

        private KonecSilnice A;
        private KonecSilnice B;
        public List<KonecSilnice> konce;
        public Edge edge;

        private IDStruct idStruct;

        public override void setID(IDStruct id)
        {
            this.idStruct = id;
        }

        public override int getID()
        {
            return this.idStruct.id;
        }

        public override Vector3 getVectorID()
        {
            return this.idStruct.idVector;
        }

        public override void setEdge(Edge e)
        {
            this.edge = e;
        }

        /// <summary>
        /// Maximální rychlost ve zvoleném směru. Je to poměrně odolné vůči blbým hodnotám.
        /// </summary>
        /// <param name="smer"></param>
        /// <returns></returns>
        public override int getMaxSpeed(Vector3 smer)
        {
            if (smer == A.getDirection())
            {
                return pravyPas.getPruhy()[0].MaxSpeed;
            }
            else{
                return levyPas.getPruhy()[0].MaxSpeed;
            }
        }


        /// <summary>
        /// Doba průjezdu (s) ve zvoleném směru. Je to poměrně odolné vůči blbým hodnotám.
        /// </summary>
        /// <param name="smer"></param>
        /// <returns></returns>
        public override float getPassTime(Vector3 smer)
        {
            if (smer == A.getDirection())
            {
                return pravyPas.getPruhy()[0].getPassTime();
            }
            else
            {
                return levyPas.getPruhy()[0].getPassTime();
            }
        }
        public override float getLength(Vector3 smer)
        {
            if (smer == A.getDirection())
            {
                return pravyPas.getPruhy()[0].Length;
            }
            else
            {
                return levyPas.getPruhy()[0].Length;
            }
        }

        public override int getPrice()
        {
            return (int)(getLength() * GameLogic.RoadPrice(SilniceType.S60_40));
        }

        public override int getDestroyPrice()
        {
            return getPrice() / 5;
        }

        /// <summary>
        /// Délka oblouku asi tak průměrná pro všechny pruhy. Nevím jestli to k něčemu je - normálně se dá asi použít spíš ta, kam se cpe směr.
        /// </summary>
        /// <returns></returns>
        public override float getLength()
        {
             return (pravyPas.getPruhy()[0].Length
                + levyPas.getPruhy()[0].Length)/2;
        }

        /// <summary>
        /// Konstruktor zatáčky.
        /// </summary>
        /// <param name="A">Počáteční bod</param>
        /// <param name="B">Koncový bod</param>
        /// <param name="BtoA">Směr z B do A (pro vykreslení správné části oblouku)</param>
        /// <param name="S">Střed oblouku</param>
        /// <param name="gdevice"></param>
        /// <param name="levyPas">pruhy levého pásu</param>
        /// <param name="stredovyPas">pruhy středového pásu</param>
        /// <param name="pravyPas">pruhy pravého pásu</param>
        public Oblouk(Vector3 A, Vector3 B, Vector3 BtoA, Vector3 S, GraphicsDevice gdevice, List<Pruh> levyPas, List<Pruh> stredovyPas, List<Pruh> pravyPas)
        {
            List<PruhVOblouku> levePruhy = new List<PruhVOblouku>();
            List<PruhVOblouku> stredovePruhy = new List<PruhVOblouku>();
            List<PruhVOblouku> pravePruhy = new List<PruhVOblouku>();
            this.konce = new List<KonecSilnice>();

            this.S = S;

            

            // vygenerování úseků
            Vector3 rA = A - S;
            Vector3 rB = B - S;

            rA.Normalize();
            rB.Normalize();


            float offsetR = 0;
            float offsetL = 0;

            if (stredovyPas != null)
                foreach (Pruh pruh in stredovyPas)
                {
                    stredovePruhy.Add(
                            new PruhVOblouku(pruh, A, B, BtoA, S, ref gdevice)
                        );
                    offsetL = -pruh.getSize() / 2;
                    offsetR = pruh.getSize() / 2;
                    break; // více středových pruhů zatím nepodporujeme
                }

            if (pravyPas != null)
                foreach (Pruh pruh in pravyPas)
                {
                    offsetR += pruh.getSize() / 2;
                    pravePruhy.Add(
                            new PruhVOblouku(pruh, A + offsetR * rA, B + offsetR * rB, BtoA, S, ref gdevice)
                        );
                    offsetR += pruh.getSize() / 2;
                }

            if (levyPas != null)
                foreach (Pruh pruh in levyPas)
                {
                    offsetL -= pruh.getSize() / 2;
                    levePruhy.Add(
                            new PruhVOblouku(pruh, A + offsetL * rA, B + offsetL * rB, BtoA, S, ref gdevice)
                        );
                    offsetL -= pruh.getSize() / 2;
                }

            this.stredovyPas = new Pas(stredovePruhy);
            this.levyPas = new Pas(levePruhy);
            this.pravyPas = new Pas(pravePruhy);

            // Výpočet směru koncových bodů
            Vector3 AB = B - A;
            Vector3 dB1 = GeometryHelper.RotateAroundPoint(Vector3.Normalize(rB), Vector3.Zero, new Vector3(0, 0, 1), MathHelper.ToRadians(90));
            Vector3 dB2 = GeometryHelper.RotateAroundPoint(Vector3.Normalize(rB), Vector3.Zero, new Vector3(0, 0, 1), MathHelper.ToRadians(270));

            Vector3 dB;
            Vector3 dA;

            float op1, op2;

            op1 = (BtoA - dB1).Length();
            op2 = (BtoA - dB2).Length();

            if (op1 < op2)
            {
                dB = dB2;
                dA = GeometryHelper.RotateAroundPoint(Vector3.Normalize(rA), Vector3.Zero, new Vector3(0, 0, 1), MathHelper.ToRadians(90));
            }
            else
            {
                dB = dB1;
                dA = GeometryHelper.RotateAroundPoint(Vector3.Normalize(rA), Vector3.Zero, new Vector3(0, 0, 1), MathHelper.ToRadians(270));
            }

            this.A = new KonecSilnice(A, rA, Vector3.Normalize(dA));
            this.B = new KonecSilnice(B, rB, Vector3.Normalize(dB));

            this.konce.Add(this.A);
            this.konce.Add(this.B);
        }
        

        /// <summary>
        /// Rovná silnice - oblouk s nekonečným poloměrem.
        /// </summary>
        /// <param name="A">Počáteční bod.</param>
        /// <param name="B">Koncový bod.</param>
        /// <param name="gdevice"></param>
        /// <param name="levyPas">Pruhy levého pásu.</param>
        /// <param name="stredovyPas">Pruhy středového (dělícího) pásu.</param>
        /// <param name="pravyPas">Pruhy pravého pásu.</param>
        public Oblouk(Vector3 A, Vector3 B, GraphicsDevice gdevice, List<Pruh> levyPas, List<Pruh> stredovyPas, List<Pruh> pravyPas)
        {
            List<PruhVOblouku> levePruhy = new List<PruhVOblouku>();
            List<PruhVOblouku> stredovePruhy = new List<PruhVOblouku>();
            List<PruhVOblouku> pravePruhy = new List<PruhVOblouku>();
            this.konce = new List<KonecSilnice>();

            this.S = A;

            Vector3 nAB = Vector3.Normalize(Vector3.Cross(B - A, Vector3.Backward));



            // vygenerování úseků
            float offsetR = 0;
            float offsetL = 0;

            if (stredovyPas != null)
                foreach (Pruh pruh in stredovyPas)
                {
                    stredovePruhy.Add(
                            new PruhVOblouku(pruh, A, B, ref gdevice, -1)
                        );
                    offsetL = -pruh.getSize() / 2;
                    offsetR = pruh.getSize() / 2;
                    break; // více středových pruhů zatím nepodporujeme
                }

            if (pravyPas != null)
                foreach (Pruh pruh in pravyPas)
                {
                    offsetR += pruh.getSize() / 2;
                    pravePruhy.Add(
                            new PruhVOblouku(pruh, A + offsetR * nAB, B + offsetR * nAB, ref gdevice, -1)
                        );
                    offsetR += pruh.getSize() / 2;
                }

            if (levyPas != null)
                foreach (Pruh pruh in levyPas)
                {
                    offsetL -= pruh.getSize() / 2;
                    levePruhy.Add(
                            new PruhVOblouku(pruh, A + offsetL * nAB, B + offsetL * nAB, ref gdevice, 1)
                        );
                    offsetL -= pruh.getSize() / 2;
                }

            this.stredovyPas = new Pas(stredovePruhy);
            this.levyPas = new Pas(levePruhy);
            this.pravyPas = new Pas(pravePruhy);

            //System.Console.WriteLine("rovna " + Vector3.Normalize((A - B)));

            this.A = new KonecSilnice(A, nAB, Vector3.Normalize(A - B));
            this.B = new KonecSilnice(B, nAB, Vector3.Normalize(B - A));

            this.konce.Add(this.A);
            this.konce.Add(this.B);
        }

        /// <summary>
        /// Konstruktor šíkmého oblouku pro jednoduché propojení dvou konců silnic.
        /// </summary>
        /// <param name="A">Počáteční bod</param>
        /// <param name="B">Koncový bod</param>
        /// <param name="gdevice">GraphicsDevice</param>
        /// <param name="levyPas">pruhy levého pásu</param>
        /// <param name="stredovyPas">pruhy středového pásu</param>
        /// <param name="pravyPas">pruhy pravého pásu</param>
        public Oblouk(KonecSilnice A, KonecSilnice B, GraphicsDevice gdevice, List<Pruh> levyPas, List<Pruh> stredovyPas, List<Pruh> pravyPas)
        {
            List<PruhVOblouku> levePruhy = new List<PruhVOblouku>();
            List<PruhVOblouku> stredovePruhy = new List<PruhVOblouku>();
            List<PruhVOblouku> pravePruhy = new List<PruhVOblouku>();
            this.konce = new List<KonecSilnice>();

            this.S = A.getPosition();

            Vector3 Ap = A.getPosition();
            Vector3 Bp = B.getPosition();

            Vector3 nAB = Vector3.Normalize(Vector3.Cross(Bp - Ap, Vector3.Backward));



            // vygenerování úseků
            float offsetR = 0;
            float offsetL = 0;

            if (stredovyPas != null)
                foreach (Pruh pruh in stredovyPas)
                {
                    stredovePruhy.Add(
                            new PruhVOblouku(pruh, A, B, 0, ref gdevice)
                        );
                    offsetL = -pruh.getSize() / 2;
                    offsetR = pruh.getSize() / 2;
                    break; // více středových pruhů zatím nepodporujeme
                }

            if (pravyPas != null)
                foreach (Pruh pruh in pravyPas)
                {
                    offsetR += pruh.getSize() / 2;
                    pravePruhy.Add(
                            new PruhVOblouku(pruh, A, B, offsetR, ref gdevice)
                        );
                    offsetR += pruh.getSize() / 2;
                }

            if (levyPas != null)
                foreach (Pruh pruh in levyPas)
                {
                    offsetL -= pruh.getSize() / 2;
                    levePruhy.Add(
                            new PruhVOblouku(pruh, A, B, offsetL, ref gdevice)
                        );
                    offsetL -= pruh.getSize() / 2;
                }

            this.stredovyPas = new Pas(stredovePruhy);
            this.levyPas = new Pas(levePruhy);
            this.pravyPas = new Pas(pravePruhy);

            //System.Console.WriteLine("rovna " + Vector3.Normalize((A - B)));

            this.A = new KonecSilnice(Ap, nAB, Vector3.Normalize(Ap - Bp));
            this.B = new KonecSilnice(Bp, nAB, Vector3.Normalize(Bp - Ap));

            this.konce.Add(this.A);
            this.konce.Add(this.B);
        }

        public override void Draw(Effect effect)
        {
            if (levyPas != null)
                foreach (PruhVOblouku pruh in this.levyPas.getPruhy())
                {
                    pruh.Draw(effect);
                }
            if (stredovyPas != null)
                foreach (PruhVOblouku pruh in this.stredovyPas.getPruhy())
                {
                    pruh.Draw(effect);
                }
            if (pravyPas != null)
                foreach (PruhVOblouku pruh in this.pravyPas.getPruhy())
                {
                    pruh.Draw(effect);
                }
        }

        public KonecSilnice getA() 
        {
            return this.A;
        }

        public KonecSilnice getB()
        {
            return this.B;
        }

        public override List<KonecSilnice> getAllEnds()
        {
            return this.konce;
        }

        /// <summary>
        /// Vrátí konec silnice nejblíž souřadnicím.
        /// </summary>
        /// <param name="x">Testované souřadnice</param>
        /// <returns>Nejbližší konec</returns>
        public override KonecSilnice getKonec(Vector3 x)
        {
            if ((x - B.getPosition()).Length() < (x - A.getPosition()).Length())
            {
                return B;
            }
            return A;
        }

        /// <summary>
        /// Vrací střed oblouku. V případě nekonečného poloměru je S = A.getPosition().
        /// </summary>
        /// <returns>Střed oblouku.</returns>
        public Vector3 getS()
        {
            return this.S;
        }
    }
}
