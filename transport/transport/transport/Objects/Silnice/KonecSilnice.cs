﻿using Microsoft.Xna.Framework;

namespace transport
{
    public class KonecSilnice
    {
        Vector3 X;
        Vector3 normal;
        Vector3 direction = Vector3.Zero;
        public Vertex vertex;

        public KonecSilnice(Vector3 X, Vector3 normal)
        {
            this.X = X;
            this.normal = normal;
        }

        public KonecSilnice(Vector3 X, Vector3 normal, Vector3 direction)
        {
            this.X = X;
            this.normal = normal;
            this.direction = direction;
        }

        public Vector3 getPosition()
        {
            return X;
        }

        public Vector3 getNormal()
        {
            return normal;
        }

        public Vector3 getDirection()
        {
            return direction;
        }
        public void reset()
        {
            this.direction = Vector3.Zero;
            this.normal = Vector3.Zero;
            this.X = Vector3.Zero;
        }

        public void setVertex (Vertex vertex)
        {
            this.vertex = vertex;
        }
    }
}
