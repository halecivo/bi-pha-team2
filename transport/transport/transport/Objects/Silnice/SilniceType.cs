﻿using System.Collections.Generic;

namespace transport
{
    class SilniceType
    {
        public static ObloukBuilder S60_40, S65_60, S75_70;

        static SilniceType()
        {
            initS60_40();
            initS65_60();
            initS75_70();
        }

        static void initS60_40()
        {
            List<Pruh> pruhy = new List<Pruh>();
            List<Pruh> stred = new List<Pruh>();

            pruhy.Add(PruhType.A300);
            pruhy.Add(PruhType.PLNA);

            stred.Add(PruhType.PRERUSOVANA);

            S60_40 = new ObloukBuilder(pruhy, stred, pruhy);
        }

        static void initS65_60()
        {
            List<Pruh> pruhy = new List<Pruh>();
            List<Pruh> stred = new List<Pruh>();

            pruhy.Add(PruhType.A275);
            pruhy.Add(PruhType.PLNA);
            pruhy.Add(PruhType.E050);

            stred.Add(PruhType.PRERUSOVANA);

            S65_60 = new ObloukBuilder(pruhy, stred, pruhy);
        }

        static void initS75_70()
        {
            List<Pruh> pruhy = new List<Pruh>();
            List<Pruh> stred = new List<Pruh>();

            pruhy.Add(PruhType.A300);
            pruhy.Add(PruhType.PLNA);
            pruhy.Add(PruhType.E050);

            stred.Add(PruhType.PRERUSOVANA);

            S75_70 = new ObloukBuilder(pruhy, stred, pruhy);
        }

    }
}
