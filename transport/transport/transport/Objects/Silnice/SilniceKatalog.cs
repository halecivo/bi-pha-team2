﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using transport.GUI;

namespace transport
{
    /// <summary>
    /// Katalog (databáze) všech postavených silnic ve hře. Umožňuje stavět a bourat silnice.
    /// </summary>
    class SilniceKatalog
    {
        List<Silnice> silnice = new List<Silnice>();
        GraphicsDevice gdevice;
        public bool collisionChecked = true;
        private List<int> collisionTestBuffer = new List<int>();
        Paths paths;
        Dijkstra dijkstra;
        QuestLog log;
        QuestWindow questWindow;
        public bool showRoadLoad;

        //docasne
        const bool dijkstra_debug = false;

        public GUI.Tools activeTool;
        public bool build = false;

        #region Konstruktory
        public SilniceKatalog(GraphicsDevice gdevice, Paths paths, bool showRoadLoad, Dijkstra dijkstra, QuestLog log, QuestWindow questWindow)
        {
            this.gdevice = gdevice;
            this.paths = paths;
            this.showRoadLoad = showRoadLoad;
            this.dijkstra = dijkstra;
            this.log = log;
            this.questWindow = questWindow;
        }
        #endregion

        #region Pomocné funkce
        private Vector3 findS(KonecSilnice Ak, Vector3 B)
        {
            Vector3 nA = Ak.getNormal();
            Vector3 A = Ak.getPosition();
            Vector3 nO = GeometryHelper.VectorPerpendicular(B - A, new Vector3(0, 0, 1));
            Vector3 O = (B + A)/2;

            // pokud lze spojit přímkou, vrátíme místro středu A
            if(GeometryHelper.UnsignedAngleBetweenTwoV3(Ak.getDirection(), B-A) < MathHelper.ToRadians(0.001f) ){
                return A;
            }

            return GeometryHelper.LineIntersection(A, nA, O, nO);
            
        }
        #endregion

        #region Operace nad množinou silnic
        /// <summary>
        /// Zamkne všechny silnice proti zbourání uživatelem.
        /// </summary>
        public void LockAll(){
            foreach (Silnice s in silnice)
            {
                s.deletable = false;
            }
        }

        /// <summary>
        /// Postaví všechny silnice definitvně (jejich zbourání bude něco stát). Vhodné spustit na začátku hry a po splnění každého úkolu.
        /// </summary>
        public void BuildAll()
        {
            foreach (Silnice s in silnice)
            {
                s.built = true;
            }
        }

        public Silnice get(int id)
        {
            foreach (Silnice s in silnice)
            {
                if (s.getID() == id)
                    return s;
            }
            throw new System.ArgumentOutOfRangeException("Neplatné ID silnice (" + id + ").");
        }
        #endregion

        #region Stavění křižovatek
        /// <summary>
        /// Postaví křižovatku ke konci nějaké silnice.
        /// </summary>
        /// <param name="A">Konec silnice k navázání.</param>
        /// <param name="rameno">Rameno křižovatky.</param>
        public void BuildCross(KonecSilnice A, KrizovatkaType type = KrizovatkaType.T, int rameno = 0)
        {
            Krizovatka k;
            KonecSilnice a, b;
            
            int ii = 0;
            IDStruct id = new IDStruct();
            
            switch (type)
            {
                case KrizovatkaType.T:
                    k = KrizovatkaFactory.T(A, rameno).Create(gdevice);
                    silnice.Add(k);
                    break;
                case KrizovatkaType.Kriz:
                    k = KrizovatkaFactory.Kriz(A, rameno).Create(gdevice);
                    silnice.Add(k);
                    break;
                default:
                    return;
            }
            id = IDGenerator.newID();

            collisionChecked = false;
            collisionTestBuffer.Add(id.id);
            silnice[silnice.Count - 1].setID(id);

            // propojit vsechny uzly hranami
            for (int i = 0; i < k.konce.Count; i++) {
                for (int j = 0; j < k.konce.Count; j++) {
                    if (i != j) {
                        a = k.konce[i];
                        b = k.konce[j];
                        addToGraph(a, b, id.id, k);        
                    }
                }
            }
            if (dijkstra.run)
            {
                updateQuestLog();
            }
        }
        #endregion

        #region Stavění předzpracovaných silnic
        /// <summary>
        /// Postaví naprojektovanou silnici.
        /// </summary>
        /// <param name="A">Počáteční bod.</param>
        /// <param name="B">Koncový bod.</param>
        /// <param name="S">Střed oblouku.</param>
        /// <param name="type">Typ silnice.</param>
        public void BuildProjectedRoad(Vector3 A, Vector3 B, Vector3 BtoA, Vector3 S, ObloukBuilder type)
        {
            Oblouk o = type.Create(A, B, BtoA, S, gdevice);
            KonecSilnice a = o.getA();
            KonecSilnice b = o.getB();
            IDStruct id = IDGenerator.newID();
            
            silnice.Add(o);
            collisionChecked = false;
            collisionTestBuffer.Add(id.id);
            silnice[silnice.Count - 1].setID(id);
            
            addToGraph(a, b, id.id, o);
            addToGraph(b, a, id.id, o);

            updateQuestLog();
        }

        /// <summary>
        /// Postaví naprojektovanou rovnou silnici.
        /// </summary>
        /// <param name="A">Počáteční bod.</param>
        /// <param name="B">Koncový bod.</param>
        /// <param name="type">Typ silnice.</param>
        public void BuildProjectedRoad(Vector3 A, Vector3 B, ObloukBuilder type)
        {
            Oblouk o = type.Create(A, B, gdevice);
            KonecSilnice a = o.getA();
            KonecSilnice b = o.getB();
            IDStruct id = IDGenerator.newID();
            
            silnice.Add(o);
            collisionChecked = false;
            collisionTestBuffer.Add(id.id);
            silnice[silnice.Count - 1].setID(id);

            addToGraph(a, b, id.id, o);
            addToGraph(b, a, id.id, o);

            updateQuestLog();
        }

        /// <summary>
        /// Postaví šikmé propojení dvou silnic - ošklivé, ale funkční.
        /// </summary>
        /// <param name="A">Konec první silnice</param>
        /// <param name="B">Konec druhé silnice</param>
        /// <param name="type">typ silnice</param>
        public void BuildProjectedRoad(KonecSilnice A, KonecSilnice B, ObloukBuilder type)
        {
            Oblouk o = type.Create(A, B, gdevice); 
            KonecSilnice a = o.getA();
            KonecSilnice b = o.getB();
            IDStruct id = IDGenerator.newID();

            silnice.Add(o);
            collisionChecked = false;
            collisionTestBuffer.Add(id.id);
            silnice[silnice.Count - 1].setID(id);

            addToGraph(a, b, id.id, o);
            addToGraph(b, a, id.id, o);

            updateQuestLog();
        }
        #endregion

        #region Předzpracování a postavení silnic

        /// <summary>
        /// Vymaže naposled postavené silnice, protože kolidují.
        /// </summary>
        public void RejectCollisionTestBuffer()
        {
            foreach (int i in collisionTestBuffer)
            {
                DestroyRoad(i);
            }

            collisionTestBuffer.Clear();
        }

        /// <summary>
        /// Vyprázní buffer naposled postavených silnic - nekolidují.
        /// </summary>
        public void AcceptCollisionTestBuffer()
        {
            collisionTestBuffer.Clear();
        }

        /// <summary>
        /// Naváže na konec silnice další silnici.
        /// </summary>
        /// <param name="A">Konec předchozí silnice.</param>
        /// <param name="B">Cílový koncový bod nového úseku.</param>
        /// <param name="type">Typ silnice.</param>
        public void BuildRoad(KonecSilnice A, Vector3 B, ObloukBuilder type)
        {
            Vector3 S = findS(A, B);
            if (S == A.getPosition())
            {
                BuildProjectedRoad(A.getPosition(), B, type);
            }
            else
            {
                BuildProjectedRoad(B, A.getPosition(), A.getDirection(), S, type);
            }
        }

        /// <summary>
        /// Postaví volnou silnici procházející třemi body.
        /// </summary>
        /// <param name="A">Počáteční bod.</param>
        /// <param name="B">Koncový bod.</param>
        /// <param name="C">Průchozí bod.</param>
        /// <param name="type">Typ silnice.</param>
        public void BuildRoad(Vector3 A, Vector3 B, Vector3 C, ObloukBuilder type)
        {

            if (A == B && B == C)
                throw new System.ArgumentOutOfRangeException("Nelze postavit silnici danou třemi totožnými body");


            // A, B budou koncové body, C průsečný
            float AB, BC, AC;
            AB = (A - B).Length();
            BC = (B - C).Length();
            AC = (A - C).Length();

            if (AC >= AB && AC >= BC)
            {
                C += B;
                B = C - B;
                C = C - B;
            }
            else if (BC >= AB && BC >= AC)
            {
                C += A;
                A = C - A;
                C = C - A;
            }

            // rovný úsek?
            if ( Vector3.Normalize(B - A) == Vector3.Normalize(C - A) )
            {
                BuildProjectedRoad(A, B, type);
                return;
            }

            // nalezení středu
            Vector3 S = GeometryHelper.FindS(A, B, C);
            Vector3 BtoC = C - B;


            Vector3 op1 = GeometryHelper.VectorPerpendicular(S - B, Vector3.Cross(A-B,C-B));
            Vector3 op2 = GeometryHelper.VectorPerpendicular(S - B, -Vector3.Cross(A-B,C-B));

            Vector3 BtoA;

            if ((BtoC - op1).Length() > (BtoC - op2).Length())
            {
                BtoA = op2;
            }
            else
            {
                BtoA = op1;
            }

            BuildProjectedRoad(A, B, BtoA, S, type);
        }

        /// <summary>
        /// Propojí dva konce silnic dvěma oblouky.
        /// </summary>
        /// <param name="A">Konec první silnice.</param>
        /// <param name="B">Konec druhé silnice.</param>
        /// <param name="type">Typ silnice.</param>
        public void BuildRoad(KonecSilnice A, KonecSilnice B, ObloukBuilder type, int mmm)
        {
            //lze spojit rovným úsekem
            float unsignedAngle = GeometryHelper.UnsignedAngleBetweenTwoV3(A.getDirection(), B.getPosition() - A.getPosition());
            if ( unsignedAngle < MathHelper.ToRadians(0.001f) )
            {
                BuildProjectedRoad(A.getPosition(), B.getPosition(), type);
                return;
            }



            Vector3 Ap = A.getPosition(), Bp = B.getPosition();

            Vector3 R = (Ap + Bp) / 2;

            Vector3 An = Vector3.Normalize( Vector3.Cross(Bp - Ap, A.getDirection()) );
            Vector3 Bn = Vector3.Normalize( Vector3.Cross(Ap - Bp, B.getDirection()) );

            Vector3 OAP = GeometryHelper.VectorPerpendicular(R - Ap, An);
            Vector3 OBP = GeometryHelper.VectorPerpendicular(R - Bp, Bn);

            Vector3 SA = GeometryHelper.LineIntersection(Ap, A.getNormal(), (Ap + R) / 2, OAP);
            Vector3 SB = GeometryHelper.LineIntersection(Bp, B.getNormal(), (Bp + R) / 2, OBP);

            Vector3 P =  (SA + SB) / 2;



            if (An.Z * Bn.Z > 0)
            {


                Vector3 M = Ap;
                Vector3 N = Bp;

                Vector3 Av = Vector3.Normalize(A.getNormal())/100;
                Vector3 Bv = Vector3.Normalize(B.getNormal())/100;

                Vector3 prusecikNormal;

                int i = 0;
                while (true)
                {
                    M += Av;
                    N -= Bv;

                    if ((2 * (Ap - M).Length() - (M - N).Length()) < 0.01f && (2 * (Ap - M).Length() - (M - N).Length()) > -0.01f)
                    {
                        System.Console.WriteLine("Vektorový součin: " + Vector3.Cross(GeometryHelper.VectorZ(A.getDirection(), 0), GeometryHelper.VectorZ(B.getDirection(), 0)).Z);
                        if (Vector3.Cross(GeometryHelper.VectorZ(A.getDirection(), 0), GeometryHelper.VectorZ(B.getDirection(), 0)).Z < 0)
                            break;
                        Vector3 SMN = (M + N) / 2;
                        BuildProjectedRoad(SMN, Ap, A.getDirection(), M, type);
                        BuildProjectedRoad(SMN, Bp, B.getDirection(), N, type);
                        return;
                    }
                    i++;
                    if (i > 1000000)
                    {
                        break;
                    }

                }
                M = Ap;
                N = Bp;
                i = 0;
                while (true)
                {
                    M -= Av;
                    N += Bv;

                    if ((2 * (Ap - M).Length() - (M - N).Length()) < 0.01f && (2 * (Ap - M).Length() - (M - N).Length()) > -0.01f)
                    {
                        Vector3 SMN = (M + N) / 2;
                        BuildProjectedRoad(SMN, Ap, A.getDirection(), M, type);
                        BuildProjectedRoad(SMN, Bp, B.getDirection(), N, type);
                        return;
                    }
                    i++;
                    if (i > 1000000)
                    {
                        return;
                    }

                }

                try
                {
                    prusecikNormal = GeometryHelper.LineIntersection(Ap, Av, Bp, Av);
                }
                catch (ArithmeticException e)
                {
                    BuildProjectedRoad(P, Ap, A.getDirection(), SA, type);
                    BuildProjectedRoad(P, Bp, B.getDirection(), SB, type);
                    return;
                }
                Vector3 stredAB = (Ap + Bp) / 2;
                Vector3 stredovyVektor;

                Vector3 stredRozdilu;

                Vector3 spojniceA = -(Ap - prusecikNormal);
                Vector3 spojniceB = -(Bp - prusecikNormal);
                Av = Vector3.Normalize(spojniceA);
                Bv = Vector3.Normalize(spojniceB);

                float r;
                float angle;




                if (spojniceA.Length() > spojniceB.Length())
                {
                    stredRozdilu = Ap + Av * (spojniceA.Length() - spojniceB.Length() / 2);

                    stredovyVektor = Vector3.Normalize(stredRozdilu - stredAB);
                    angle = GeometryHelper.UnsignedAngleBetweenTwoV3(-stredovyVektor, -Av);
                    float angleSin = (float)Math.Sin((double)angle);
                    r = ((stredRozdilu - Ap).Length() * angleSin) / (1 + angleSin);

                    SA = Ap + Av * r;
                    SB = Bp + Bv * r;
                }
                else
                {
                    stredRozdilu = Bp + Bv * -(spojniceA.Length() - spojniceB.Length() / 2);

                    stredovyVektor = Vector3.Normalize(stredRozdilu - stredAB);
                    angle = GeometryHelper.UnsignedAngleBetweenTwoV3(-stredovyVektor, -Bv);
                    float angleSin = (float)Math.Sin((double)angle);
                    r = ((stredRozdilu - Bp).Length() * angleSin) / (1 + angleSin);

                    SA = Ap + Av * r;
                    SB = Bp + Bv * r;
                }


                BuildProjectedRoad((SA + SB) / 2, Ap, A.getDirection(), SA, type);
                BuildProjectedRoad((SA + SB) / 2, Bp, B.getDirection(), SB, type);


            }
            else
            {
                Vector3 SAB = SB - SA;

                Vector3 translateS = Vector3.Normalize(GeometryHelper.VectorPerpendicular(SAB, An));


                Vector3 translateSA = translateS * (SA - Ap).Length();
                Vector3 translateSB = translateS * (SB - Bp).Length();

                Vector3 RA = SA + translateSA, RB = SB + translateSB;
                if ((Bp - (Ap + A.getDirection())).Length() < (Ap - (Bp + B.getDirection())).Length())
                {
                    BuildProjectedRoad(Bp + 2 * (SB - Bp), Bp, B.getDirection(), SB, type);
                    BuildRoad(A, new KonecSilnice(Bp + 2 * (SB - Bp), Vector3.Normalize(SB - Bp), -1 * B.getDirection()), type);
                }
                else
                {
                    BuildProjectedRoad(Ap + 2 * (SA - Ap), Ap, A.getDirection(), SA, type);
                    BuildRoad(B, new KonecSilnice(Ap + 2 * (SA - Ap), Vector3.Normalize(SA - Ap), -1 * A.getDirection()), type);
                }
                //BuildProjectedRoad(RA, RB, type);
                //BuildProjectedRoad(RB, Bp, B.getDirection(), SB, type);
            }
        }

        // tuto metodu nepouzivat, stejne nefunguje. nechavam jen kdybych se k tomu chtel vratit
        public void BuildRoad(KonecSilnice A, KonecSilnice B, ObloukBuilder type, float dd)
        {
            //lze spojit rovným úsekem
            float unsignedAngle = GeometryHelper.UnsignedAngleBetweenTwoV3(A.getDirection(), B.getPosition() - A.getPosition());
            if (unsignedAngle < MathHelper.ToRadians(0.001f))
            {
                BuildProjectedRoad(A.getPosition(), B.getPosition(), type);
                return;
            }



            Vector3 Ap = A.getPosition(), Bp = B.getPosition();

            Vector3 R = (Ap + Bp) / 2;

            Vector3 An = Vector3.Normalize(Vector3.Cross(Bp - Ap, A.getDirection()));
            Vector3 Bn = Vector3.Normalize(Vector3.Cross(Ap - Bp, B.getDirection()));

            Vector3 OAP = GeometryHelper.VectorPerpendicular(R - Ap, An);
            Vector3 OBP = GeometryHelper.VectorPerpendicular(R - Bp, Bn);

            Vector3 SA = GeometryHelper.LineIntersection(Ap, A.getNormal(), (Ap + R) / 2, OAP);
            Vector3 SB = GeometryHelper.LineIntersection(Bp, B.getNormal(), (Bp + R) / 2, OBP);

            Vector3 P = (SA + SB) / 2;



            //if (An.Z * Bn.Z > 0)
            //{


                Vector3 M = Ap;
                Vector3 N = Bp;

                float r = (Ap - Bp).Length() / 2;

                // varianta 1
                M = Ap + A.getNormal() * r;

                Vector3 E = GeometryHelper.LineIntersection(M, B.getDirection(), Bp, GeometryHelper.VectorPerpendicular(B.getDirection(), new Vector3(0, 0, 1)));
                double U = (Bp - E).Length();
                double Z = (M - E).Length();

                double rB = (U * U + Z * Z - r * r) / (2 * (r + U));

                N = Bp + Vector3.Normalize(E-Bp) * (float)rB;
                P = M + Vector3.Normalize(N - M)*r;
                BuildProjectedRoad(P, Ap, A.getDirection(), M, type);
                BuildProjectedRoad(P, Bp, B.getDirection(), N, type);

                //System.Console.WriteLine("|EN| = {0}, |BN| = {1}, |PN| = {2}, |MN| = {3}, |PM| = {4}, r = {5}, rB = {6}.", (E - N).Length(), (Bp - N).Length(), (P - N).Length(), (M - N).Length(), (P - M).Length(), r, rB);
                //System.Console.WriteLine("A = {0}, B = {1}, M = {2}, N = {3}, P = {4}, E = {5}",
                 //   Ap, Bp, M, N, P, E);
                return;
                // varianta 2

                M = Ap - A.getNormal() * r;



                System.Console.WriteLine("žádná možnost se nepovedla");

            //}
            //else
            //{
            //    Vector3 SAB = SB - SA;

            //    Vector3 translateS = Vector3.Normalize(GeometryHelper.VectorPerpendicular(SAB, An));


            //    Vector3 translateSA = translateS * (SA - Ap).Length();
            //    Vector3 translateSB = translateS * (SB - Bp).Length();

            //    Vector3 RA = SA + translateSA, RB = SB + translateSB;
            //    if ((Bp - (Ap + A.getDirection())).Length() < (Ap - (Bp + B.getDirection())).Length())
            //    {
            //        BuildProjectedRoad(Bp + 2 * (SB - Bp), Bp, B.getDirection(), SB, type);
            //        BuildRoad(A, new KonecSilnice(Bp + 2 * (SB - Bp), Vector3.Normalize(SB - Bp), -1 * B.getDirection()), type);
            //    }
            //    else
            //    {
            //        BuildProjectedRoad(Ap + 2 * (SA - Ap), Ap, A.getDirection(), SA, type);
            //        BuildRoad(B, new KonecSilnice(Ap + 2 * (SA - Ap), Vector3.Normalize(SA - Ap), -1 * A.getDirection()), type);
            //    }
            //    //BuildProjectedRoad(RA, RB, type);
            //    //BuildProjectedRoad(RB, Bp, B.getDirection(), SB, type);
            //}
        }

        public void BuildRoad(KonecSilnice A, KonecSilnice B, ObloukBuilder type)
        {
            Vector3 Ap = A.getPosition(), Bp = B.getPosition(), nA = A.getNormal(), nB = B.getNormal(), sA = A.getDirection(), sB = B.getDirection();
            float r = (Ap - Bp).Length()/4;

            #region Určení středů kružnic
            Vector3 rA = r * nA;
            Vector3 rB = r * nB;
            Vector3 SA, SB;

            if ((Ap + rA - Bp).Length() > (Ap - rA - Bp).Length())
            {
                SA = Ap - rA;
            }
            else
            {
                SA = Ap + rA;
            }

            if ((Bp + rB - Ap).Length() > (Bp - rB - Ap).Length())
            {
                SB = Bp - rB;
            }
            else
            {
                SB = Bp + rB;
            }
            #endregion

            #region Určení bodů, kde na sebe kružnice koukají
            
            Vector3 P = (SA + SB) / 2;
            float crossA = Vector3.Cross(Ap - SA, P - SA).Z, crossB = Vector3.Cross(Bp - SB, P - SB).Z;

            if (crossA * crossB <= 0)
            {
                Vector3 abp = GeometryHelper.VectorPerpendicular(Vector3.Normalize(Bp - Ap), new Vector3(0,0,1))*r;
                Vector3 TA, TB;
                if (((Ap + sA) - (SA + abp)).Length() > ((Ap + sA) - (SA - abp)).Length())
                {
                    TA = SA - abp;
                    TB = SB - abp;
                }
                else
                {
                    TA = SA + abp;
                    TB = SB + abp;
                }



                BuildProjectedRoad(TA, Ap, sA, SA, type);
                BuildProjectedRoad(TA, TB, type);
                BuildProjectedRoad(TB, Bp, sB, SB, type);
            }
            else
            {

                float d = (P - SA).Length();
                double Beta = Math.Acos(r / d);
                Vector3 PA = SA + Vector3.Normalize(P - SA)*r;
                Vector3 TA = GeometryHelper.RotateAroundPoint(PA, SA, Vector3.Normalize(Vector3.Cross(P - SA, sA)), (float)Beta);

                Vector3 PB = SB + Vector3.Normalize(P - SB) * r;
                Vector3 TB = GeometryHelper.RotateAroundPoint(PB, SB, Vector3.Normalize(Vector3.Cross(P - SB, sB)), (float)Beta);



                BuildProjectedRoad(TA, Ap, sA, SA, type);
                BuildProjectedRoad(TA, TB, type);
                BuildProjectedRoad(TB, Bp, sB, SB, type);
            }


            #endregion

        }

        public void BuildConnectionRoad(KonecSilnice A, KonecSilnice B, ObloukBuilder type)
        {
            //lze spojit rovným úsekem
            float unsignedAngle = GeometryHelper.UnsignedAngleBetweenTwoV3(A.getDirection(), B.getPosition() - A.getPosition());
            if (unsignedAngle < MathHelper.ToRadians(0.001f))
            {
                BuildProjectedRoad(A.getPosition(), B.getPosition(), type);
                return;
            }

            BuildProjectedRoad(A, B, type);

        }

        #endregion

        #region Bourání
        /// <summary>
        /// Zbourá silnici, pokud není zamčena.
        /// </summary>
        /// <param name="id">Id silnici</param>
        /// <param name="force">Nebude se ohlížet na to, jestli silnice je zamčena.</param>
        public void DestroyRoad(int id, bool force = false)
        {
            List<KonecSilnice> konce;
            List<string> konce_str;
            Vertex v;

            foreach (Silnice s in silnice)
            {
                if (s.getID() == id)
                {
                    if (force == true || s.deletable == true)
                    {
                        konce = s.getAllEnds();
                        konce_str = new List<string>();

                        foreach (var k in konce) konce_str.Add(k.vertex.name);

                        foreach (var k in konce) {
                            v = k.vertex;
                            Console.WriteLine("Removing vertex " + v.name + " from road " + id);
                            v.remove(konce_str);
                        }
                        silnice.Remove(s);

                        updateQuestLog();
                     }
                    return;
                }
            }
        }

        
        #endregion

        #region Grafové funkce
        // prida do grafu dva uzly a spoji je jednou orientovanou hranou
        public void addToGraph(KonecSilnice a, KonecSilnice b, int id, Silnice s)
        {
            Vector3 apos = a.getPosition();
            Vector3 bpos = b.getPosition();

            Vertex ret = null;
            Vertex ret2 = null;
            string name;
            Vertex vert1 = null, vert2 = null;
            Vertex source = null, target = null;

            ret = paths.VertexExist(apos.X, apos.Y);
            if (ret == null)
            {
                name = IDGenerator.uniqueName();
                vert1 = new Vertex(name, paths, apos.X, apos.Y);
                paths.AddVertex(vert1);
            }

            ret2 = paths.VertexExist(bpos.X, bpos.Y);
            if (ret2 == null)
            {
                name = IDGenerator.uniqueName();
                vert2 = new Vertex(name, paths, bpos.X, bpos.Y);
                paths.AddVertex(vert2);
            }

            if (ret == null && ret2 == null)
            {
                source = vert1;
                target = vert2;
            }
            // stare uzlu uz existuji
            else if (ret != null && ret2 != null)
            {
                source = ret;
                target = ret2;
            }
            else if (ret != null && ret2 == null)
            {
                source = ret;
                target = vert2;
            }
            else // (ret == null && ret2 != null)
            {
                source = vert1;
                target = ret2;
            }

            a.setVertex(source);
            b.setVertex(target);

            // TODO: kontrola jestli dva uzly uz nejsou propojene hranami
            s.setEdge(paths.AddEdge(source, target, id, s.getMaxSpeed(Vector3.Zero), s.getPassTime(Vector3.Zero)));
        }

        public void updateQuestLog()
        {
            if (dijkstra.run) {
                dijkstra.calculatePaths(dijkstra_debug);
                float time = log.getPassTime();
                float new_time = paths.getPathCost(paths.getGoalPath().start, paths.getGoalPath().end);
                log.UpdatePassTime(new_time);

                if (new_time != time)
                {
                    //ModalWindow.ShowModal("Nová cesta", "Čas jízdy: " + new_time, true);
                    if (new_time != 0)
                    {
                        int timeSec = (int)(new_time * 1000);
                        paths.mw.ShowModal("Nová nejlepší cesta", "Čas jízdy: " + (timeSec / 60).ToString() + "m " + (timeSec % 60).ToString() + "s");
                    }
                    else
                    {
                        paths.mw.ShowModal("Varování", "Spojení do cíle neexistuje!");
                    }
                }
            }
        }

        #endregion

        #region Draw
        public void Draw(Effect effect, RTarget target, Texture2D texture = null)
        {
            switch (target)
            {
                case RTarget.Regular:
                    effect.Parameters["texturize"].SetValue(true);
                    effect.Parameters["xTexture"].SetValue(texture);
                    effect.Parameters["collision"].SetValue(false);
                    break;
                case RTarget.Back:
                    effect.Parameters["isOnBestPath"].SetValue(false);
                    effect.Parameters["clicked"].SetValue(false);
                    effect.Parameters["texturize"].SetValue(false);
                    effect.Parameters["collision"].SetValue(false);
                    break;
                case RTarget.Collision:
                    effect.Parameters["isOnBestPath"].SetValue(false);
                    effect.Parameters["clicked"].SetValue(false);
                    effect.Parameters["texturize"].SetValue(false);
                    effect.Parameters["collision"].SetValue(true);
                    break;
            }
            //HashSet<int> set = paths.getIdsAlongPath(path_start, path_end);

            //Console.Write("Ids along the road: [");
            //foreach (var v in set)
            //{
            //    Console.Write(v + ", ");
            //}
            //Console.WriteLine("]");

            int id;
            int car_count;

            foreach (Silnice s in silnice)
            {
                //id = s.getID();
                //car_count = 0;

                //List<CarPath> carPaths = paths.getColoredRoads();

                //foreach (var cp in carPaths) {
                //    if (cp.set.Contains(id)) {
                //            car_count += cp.count;
                //    }
                //}

                if (target == RTarget.Regular)
                {   
                    id = s.getID();
                    car_count = 0;
                    
                    if (showRoadLoad) {
                        List<CarPath> carPaths = paths.getColoredRoads();

                        foreach (var cp in carPaths)
                        {
                            if (cp.set.Contains(id))
                            {
                                car_count += cp.count;
                            }
                        }
                    }
                    if (activeTool == GUI.Tools.EraseTool)
                    {
                        effect.Parameters["locked"].SetValue(!s.deletable);
                    }
                    else
                    {
                        effect.Parameters["locked"].SetValue(false);
                    }

                    if (showRoadLoad)
                    {
                        effect.Parameters["car_count"].SetValue(car_count);
                    }

                    //if (set.Contains(id))
                    //{
                    //    effect.Parameters["isOnBestPath"].SetValue(true);
                    //}
                    //else
                    //{
                    //    effect.Parameters["isOnBestPath"].SetValue(false);
                    //}
                }
                else if (target == RTarget.Collision)
                {
                    if (collisionTestBuffer.Contains(s.getID()))
                    {
                        effect.Parameters["collisionSprite"].SetValue(true);
                    }
                    else
                    {
                        effect.Parameters["collisionSprite"].SetValue(false);
                    }
                }
                else // BackRT
                {
                    effect.Parameters["pickColor"].SetValue(s.getVectorID());
                }
                s.Draw(effect);
            } 
            effect.Parameters["locked"].SetValue(false);
            effect.Parameters["isOnBestPath"].SetValue(false);
            effect.Parameters["car_count"].SetValue(0);

        }
        #endregion
    }
}
