﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace transport
{
    class ObloukBuilder
    {
        private List<Pruh> pravy;
        private List<Pruh> stred;
        private List<Pruh> levy;

        public ObloukBuilder(List<Pruh> levy, List<Pruh> stred, List<Pruh> pravy)
        {
            this.pravy = pravy;
            this.stred = stred;
            this.levy = levy;
        }

      

        public Oblouk Create(Vector3 A, Vector3 B, Vector3 BtoA, Vector3 S, GraphicsDevice gdevice)
        {
            return new Oblouk(A, B, BtoA, S, gdevice, levy, stred, pravy);
        }

        public Oblouk Create(Vector3 A, Vector3 B, GraphicsDevice gdevice)
        {
            return new Oblouk(A, B, gdevice, levy, stred, pravy);
        }

        public Oblouk Create(KonecSilnice A, KonecSilnice B, GraphicsDevice gdevice)
        {
            return new Oblouk(A, B, gdevice, levy, stred, pravy);
        }

    }
}
