﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace transport
{
    enum KrizovatkaType
    {
        T,
        Kriz
    }

    class KrizovatkaFactory
    {
        public static GraphicsDeviceManager gdevice;

        static KrizovatkaFactory(){
            //T = new Krizovatka();
        }

        public static KrizovatkaBuilder makeForEach(List<KonecSilnice> konce)
        {
            KrizovatkaBuilder krizovatka = new KrizovatkaBuilder();
            float offset = (PruhType.PRAZDNA.getSize() / 2);

            for (int i = 0; i < konce.Count; i++)
            {
                krizovatka.AddKonec(konce[i]);



                for (int j = i + 1; j < konce.Count; j++)
                {
                    krizovatka.Add(
                            new PruhBuilder(PruhType.PRAZDNA, konce[i].getPosition(), konce[j].getPosition())
                        );
                }

                for (int j = 0; j < konce.Count; j++)
                {

                }
            }

            return krizovatka;
        }

        public static KrizovatkaBuilder T(KonecSilnice A, int rameno = 0)
        {

            float rotation = (float)GeometryHelper.SignedAngleBetween2DVectors(A.getDirection(), new Vector3(0,-1,0), new Vector3(1,0,0) )+MathHelper.ToRadians(90);
            Vector3 translation = A.getPosition() + Vector3.Normalize( A.getDirection())*15;
            switch (rameno)
            {
                case 0:
                    rotation += 0;
                    break;
                case 1:
                    rotation -= (float)Math.PI/2;
                    break;
                default:
                    rotation += (float)Math.PI/2;
                    break;

            }

            return T(translation, rotation);
        }

        public static KrizovatkaBuilder T(Vector3 P, float r = 0)
        {
            KrizovatkaBuilder krizovatka = new KrizovatkaBuilder();
            Pruh pruhtype = PruhType.A300;
            float offset = (PruhType.PRAZDNA.getSize() + pruhtype.getSize()) / 2;

            Vector3 S1 = new Vector3(15, -15, 0), S2 = new Vector3(15, 15, 0), S3 = new Vector3(-15, 15, 0);
            Vector3 A1 = new Vector3(0, -15, 0), A2 = new Vector3(15, 0, 0), A3 = new Vector3(0, 15, 0);
            Vector3 O1 = new Vector3(offset, 0, 0), O2 = new Vector3(0, offset, 0), O3 = new Vector3(-offset, 0, 0);

            // Z offset aby nedocházelo k zrnění
            Vector3 Z = new Vector3(0, 0, 0.001f);
            float ZS = 1;

            S1 = GeometryHelper.RotateAroundPoint(S1, Vector3.Zero, Vector3.Backward, r) + P;
            S2 = GeometryHelper.RotateAroundPoint(S2, Vector3.Zero, Vector3.Backward, r) + P;
            S3 = GeometryHelper.RotateAroundPoint(S3, Vector3.Zero, Vector3.Backward, r) + P;

            A1 = GeometryHelper.RotateAroundPoint(A1, Vector3.Zero, Vector3.Backward, r) + P;
            A2 = GeometryHelper.RotateAroundPoint(A2, Vector3.Zero, Vector3.Backward, r) + P;
            A3 = GeometryHelper.RotateAroundPoint(A3, Vector3.Zero, Vector3.Backward, r) + P;

            O1 = GeometryHelper.RotateAroundPoint(O1, Vector3.Zero, Vector3.Backward, r);
            O2 = GeometryHelper.RotateAroundPoint(O2, Vector3.Zero, Vector3.Backward, r);
            O3 = GeometryHelper.RotateAroundPoint(O3, Vector3.Zero, Vector3.Backward, r);

            krizovatka.Add(
                        new PruhBuilder(PruhType.PRAZDNA, A1 + Z*ZS, A3 + Z*ZS)
                    );
            ZS++;
            krizovatka.Add(
                        new PruhBuilder(PruhType.PRAZDNA, A1 + Z * ZS, A2 + Z * ZS, S1 + Z * ZS)
                    );
            ZS++;
            krizovatka.Add(
                        new PruhBuilder(PruhType.PRAZDNA, A3 + Z * ZS, A2 + Z * ZS, S2 + Z * ZS)
                    );
            ZS++;

            krizovatka.Add(new PruhBuilder(PruhType.PLNA, A1 + O1 * 2 + Z * ZS, A2 - O2 * 2 + Z * ZS, S1 + Z * ZS));
            ZS++;
            krizovatka.Add(new PruhBuilder(PruhType.PLNA, A2 + O2 * 2 + Z * ZS, A3 - O3 * 2 + Z * ZS, S2 + Z * ZS));
            ZS++;
            krizovatka.Add(new PruhBuilder(PruhType.PLNA, A3 + O3 * 2 + Z * ZS, A1 - O1 * 2 + Z * ZS));

            ZS++;
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A1 + O1 + Z * ZS, A2 - O2 + Z * ZS, S1 + Z * ZS, 1)
                    );
            ZS++;
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A1 + O1 + Z * ZS, A3 - O3 + Z * ZS)
                    );

            ZS++;
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A3 + O3 + Z * ZS, A2 - O2 + Z * ZS, S2 + Z * ZS, 1)
                    );
            ZS++;
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A3 + O3 + Z * ZS, A1 - O1 + Z * ZS)
                    );

            ZS++;
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A2 + O2 + Z * ZS, A3 - O3 + Z * ZS, S2 + Z * ZS, 1)
                    );
            ZS++;
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A2 + O2 + Z * ZS, A1 - O1 + Z * ZS, S1 + Z * ZS, 1)
                    );

            krizovatka.AddKonec(
                    new KonecSilnice(A1, Vector3.Normalize(O1), Vector3.Normalize(A1 - P))
                );
            krizovatka.AddKonec(
                    new KonecSilnice(A2, Vector3.Normalize(O2), Vector3.Normalize(A2 - P))
            );
            krizovatka.AddKonec(
                    new KonecSilnice(A3, Vector3.Normalize(O3), Vector3.Normalize(A3 - P))
            );


            return krizovatka;
        }

        public static KrizovatkaBuilder Kriz(KonecSilnice A, int rameno = 0)
        {

            float rotation = GeometryHelper.UnsignedAngleBetweenTwoV3(A.getDirection(), new Vector3(1, 0, 0));
            Vector3 translation = A.getPosition() + A.getDirection() * 15;
            switch (rameno)
            {
                case 0:
                    rotation += 0;
                    break;
                case 1:
                    rotation -= (float)Math.PI / 2;
                    break;
                case 2:
                    rotation -= (float)Math.PI;
                    break;
                default:
                    rotation += (float)Math.PI / 2;
                    break;

            }
            return Kriz(translation, rotation);
        }


        public static KrizovatkaBuilder Kriz(Vector3 P, float r = 0){

            KrizovatkaBuilder krizovatka = new KrizovatkaBuilder();
            Pruh pruhtype = PruhType.A300;
            float offset = (PruhType.PRAZDNA.getSize() + pruhtype.getSize()) / 2;

            Vector3 S1 = new Vector3(15, -15, 0), S2 = new Vector3(15, 15, 0), S3 = new Vector3(-15, 15, 0), S4 = new Vector3(-15, -15, 0);
            Vector3 A1 = new Vector3(0, -15, 0), A2 = new Vector3(15, 0, 0), A3 = new Vector3(0, 15, 0), A4 = new Vector3(-15, 0, 0);
            Vector3 O1 = new Vector3(offset, 0, 0), O2 = new Vector3(0, offset, 0), O3 = new Vector3(-offset, 0, 0), O4 = new Vector3(0, -offset, 0);

            S1 = GeometryHelper.RotateAroundPoint(S1, Vector3.Zero, Vector3.Backward, r) + P;
            S2 = GeometryHelper.RotateAroundPoint(S2, Vector3.Zero, Vector3.Backward, r) + P;
            S3 = GeometryHelper.RotateAroundPoint(S3, Vector3.Zero, Vector3.Backward, r) + P;
            S4 = GeometryHelper.RotateAroundPoint(S4, Vector3.Zero, Vector3.Backward, r) + P;

            A1 = GeometryHelper.RotateAroundPoint(A1, Vector3.Zero, Vector3.Backward, r) + P;
            A2 = GeometryHelper.RotateAroundPoint(A2, Vector3.Zero, Vector3.Backward, r) + P;
            A3 = GeometryHelper.RotateAroundPoint(A3, Vector3.Zero, Vector3.Backward, r) + P;
            A4 = GeometryHelper.RotateAroundPoint(A4, Vector3.Zero, Vector3.Backward, r) + P;

            O1 = GeometryHelper.RotateAroundPoint(O1, Vector3.Zero, Vector3.Backward, r);
            O2 = GeometryHelper.RotateAroundPoint(O2, Vector3.Zero, Vector3.Backward, r);
            O3 = GeometryHelper.RotateAroundPoint(O3, Vector3.Zero, Vector3.Backward, r);
            O4 = GeometryHelper.RotateAroundPoint(O4, Vector3.Zero, Vector3.Backward, r);

            krizovatka.Add(
                        new PruhBuilder(PruhType.PRAZDNA, A1, A3)
                    );
            krizovatka.Add(
                        new PruhBuilder(PruhType.PRAZDNA, A1, A2, S1)
                    );
            krizovatka.Add(
                        new PruhBuilder(PruhType.PRAZDNA, A1, A4, S4)
                    );
            krizovatka.Add(
                        new PruhBuilder(PruhType.PRAZDNA, A3, A2, S2)
                    );
            krizovatka.Add(
                        new PruhBuilder(PruhType.PRAZDNA, A3, A4, S3)
                    );
            krizovatka.Add(
                        new PruhBuilder(PruhType.PRAZDNA, A4, A2)
                    );

            krizovatka.Add(new PruhBuilder(PruhType.PLNA, A1 + O1 * 2, A2 - O2 * 2, S1));
            krizovatka.Add(new PruhBuilder(PruhType.PLNA, A2 + O2 * 2, A3 - O3 * 2, S2));
            krizovatka.Add(new PruhBuilder(PruhType.PLNA, A3 + O3 * 2, A4 - O4 * 2, S3));
            krizovatka.Add(new PruhBuilder(PruhType.PLNA, A4 + O4 * 2, A1 - O1 * 2, S4));

            krizovatka.Add(
                    new PruhBuilder(pruhtype, A1 + O1, A4 - O4, S4, -1)
                    );
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A1 + O1, A2 - O2, S1, 1)
                    );
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A1 + O1, A3 - O3)
                    );

            krizovatka.Add(
                    new PruhBuilder(pruhtype, A3 + O3, A4 - O4, S3, 1)
                    );
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A3 + O3, A2 - O2, S2, 1)
                    );
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A3 + O3, A1 - O1)
                    );

            krizovatka.Add(
                    new PruhBuilder(pruhtype, A4 + O4, A1 - O1, S4, 1)
                    );
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A4 + O4, A2 - O2, 1)
                    );
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A4 + O4, A3 - O3, S3)
                    );

            krizovatka.Add(
                    new PruhBuilder(pruhtype, A2 + O2, A3 - O3, S2, 1)
                    );
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A2 + O2, A1 - O1, S1, 1)
                    );
            krizovatka.Add(
                    new PruhBuilder(pruhtype, A2 + O2, A4 - O4)
                    );

            krizovatka.AddKonec(
                    new KonecSilnice(A1, Vector3.Normalize(O1), Vector3.Normalize(A1 - P))
                );
            krizovatka.AddKonec(
                    new KonecSilnice(A2, Vector3.Normalize(O2), Vector3.Normalize(A2 - P))
            );
            krizovatka.AddKonec(
                    new KonecSilnice(A3, Vector3.Normalize(O3), Vector3.Normalize(A3 - P))
            );
            krizovatka.AddKonec(
                    new KonecSilnice(A4, Vector3.Normalize(O4), Vector3.Normalize(A4 - P))
            );

            return krizovatka;
        }

    }
}
