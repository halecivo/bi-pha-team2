﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace transport
{
    /**
     * Trida vykresluje jednu budovu s uvedenymi vlastnostmi
     */
    class Building
    {
        // vlastnosti budovy
        private const float sqrtDve = 1.414213562373095f;
        private Model buildingModel;
        public Matrix world;
        public Vector2[] corners = new Vector2[4];
        public BoundingSphere boundingSphere;
        private GeometricBuffer<VertexPositionNormalTexture> collisionbox;
        GraphicsDevice gdevice;

        protected float scale = 1.0f;

        public Building(
            Vector3 position, Model buildingModel, Matrix projectionMatrix, GraphicsDevice gdevice, float scale = 1.0f, float rotationInDegrees = 0.0f)
        {
            this.buildingModel = buildingModel;
            this.scale = scale;
            float colScale = 1.5f;

            this.world = Matrix.Identity;
            this.world *= Matrix.CreateRotationZ(MathHelper.ToRadians(rotationInDegrees));
            this.world *= Matrix.CreateScale(this.scale);
            this.world *= Matrix.CreateTranslation(position);

            VertexPositionNormalTextureGeometricBufferFactory factory = new VertexPositionNormalTextureGeometricBufferFactory();
            Vector3 A = new Vector3(-scale * colScale, scale * colScale, 0.05f) + position,
                B = new Vector3(scale * colScale, scale * colScale, 0.05f) + position,
                C = new Vector3(scale * colScale, -scale * colScale, 0.05f) + position,
                D = new Vector3(-scale * colScale, -scale * colScale, 0.05f) + position;

            Vector3 rotAxis = new Vector3(0, 0, 1);
            A = GeometryHelper.RotateAroundPoint(A, Vector3.Zero, rotAxis, MathHelper.ToRadians(rotationInDegrees));
            B = GeometryHelper.RotateAroundPoint(B, Vector3.Zero, rotAxis, MathHelper.ToRadians(rotationInDegrees));
            C = GeometryHelper.RotateAroundPoint(C, Vector3.Zero, rotAxis, MathHelper.ToRadians(rotationInDegrees));
            D = GeometryHelper.RotateAroundPoint(D, Vector3.Zero, rotAxis, MathHelper.ToRadians(rotationInDegrees));
            
            factory.AddPane(
                A, new Vector2(0, 1), new Vector3(0, 0, 1),
                B, new Vector2(1, 1), new Vector3(0, 0, 1),
                C, new Vector2(0, 0), new Vector3(0, 0, 1),
                D, new Vector2(1, 0), new Vector3(0, 0, 1)
                );

            this.gdevice = gdevice;
            collisionbox = factory.Create(gdevice);


            this.boundingSphere = new BoundingSphere(position + new Vector3(0, 0, scale), scale * 2f);
        }

        public void Draw(Camera camera)
        {

            Matrix[] transforms = new Matrix[this.buildingModel.Bones.Count];
            this.buildingModel.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in this.buildingModel.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * this.world;
                    effect.View = camera.viewMatrix;
                    effect.Projection = camera.projectionMatrix;
                }
                mesh.Draw();
            }
        }

        public void Draw(Effect shader, RTarget RT)
        {

            ////Matrix[] transforms = new Matrix[this.buildingModel.Bones.Count];
            ////this.buildingModel.CopyAbsoluteBoneTransformsTo(transforms);

            //foreach (ModelMesh mesh in this.buildingModel.Meshes)
            //{
            //    foreach (BasicEffect effect in mesh.Effects)
            //    {
            //        effect.EnableDefaultLighting();
            //        effect.World = transforms[mesh.ParentBone.Index] * this.world;
            //        effect.View = camera.viewMatrix;
            //        effect.Projection = camera.projectionMatrix;
            //    }
            //    mesh.Draw();
            //}
            if (RT == RTarget.Regular)
            {
                Matrix shaderWorld = shader.Parameters["World"].GetValueMatrix();
                Matrix[] transforms = new Matrix[this.buildingModel.Bones.Count];
                this.buildingModel.CopyAbsoluteBoneTransformsTo(transforms);

                foreach (ModelMesh mesh in this.buildingModel.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.World = transforms[mesh.ParentBone.Index] * this.world;
                        effect.View = shader.Parameters["View"].GetValueMatrix();
                        effect.Projection = shader.Parameters["Projection"].GetValueMatrix();
                    }
                    // depth test
                    gdevice.DepthStencilState = DepthStencilState.Default;
                    gdevice.BlendState = BlendState.AlphaBlend;
                    mesh.Draw();
                }
            }
            else if (RT == RTarget.Collision)
            {
                collisionbox.Draw(shader);
            }
        }

        public void DrawToRT(RenderTarget2D RT, Camera camera)
        {
            
        }

        public virtual bool CollidesWith(Model otherModel, Matrix otherWorld)
        {
            foreach (ModelMesh myModelMeshes in buildingModel.Meshes)
            {
                foreach (ModelMesh hisModelMeshes in otherModel.Meshes)
                {
                    if (myModelMeshes.BoundingSphere.Transform(
                        this.world).Intersects(
                        hisModelMeshes.BoundingSphere.Transform(otherWorld)))
                        return true;
                }
            }
            return false;
        }

        /*
        private void computeBoundingBox()
        { //vypocet pozice rohu budovy vuci terenu
            for (int i = 0; i < 4; i++)
            {
                this.corners[i].X = this.position.X + (float)Math.Cos((double)(MathHelper.ToRadians(45.0f + (float)(90 * i)) + this.rotation) * (5.0f * sqrtDve * this.scale));
                this.corners[i].Y = this.position.Y + (float)Math.Sin((double)(MathHelper.ToRadians(45.0f + (float)(90 * i)) + this.rotation) * (5.0f * sqrtDve * this.scale));
            }
        }
         */
    }
}
