﻿using System.Collections.Generic;

namespace transport
{
    public class CarPath
    {
        public int count;
        public string start;
        public string end;
        public HashSet<int> set;

        public CarPath (string start, string end, int count)
        {
            this.start = start;
            this.end = end;
            this.count = count;
        }

        public void updateSet (HashSet<int> set)
        {
            this.set = set;
        }
    }
}
