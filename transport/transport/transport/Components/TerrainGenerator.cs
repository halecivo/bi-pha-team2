﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace transport
{
    class TerrainGenerator
    {
        GeometricBuffer<VertexPositionNormalTexture> buffer;
        //Texture2D terrainTexture;
        public BoundingBox boundingBox;

        /// <summary>
        /// Připraví terén z trojúhelníků.
        /// </summary>
        /// <param name="gdevice"></param>
        /// <param name="terrainTexture">textura terénu</param>
        /// <param name="minCorner">sořadnice levého dolního rohu mapy</param>
        /// <param name="maxCorner">souřadníce pravého horního rohu mapy</param>
        /// <param name="step">délka strany jednoho čtverečku terénu</param>
        public TerrainGenerator(GraphicsDevice gdevice, Texture2D terrainTexture, Vector2 minCorner, Vector2 maxCorner, float step = 0.1f)
        {
            VertexPositionNormalTextureGeometricBufferFactory factory = new VertexPositionNormalTextureGeometricBufferFactory();
            //this.terrainTexture = terrainTexture;

            for (float y = minCorner.Y; y < maxCorner.Y; y += step)
            {

                for (float x = minCorner.X; x < maxCorner.X; x += step) {
                    factory.AddTriangle(
                        new Vector3(x, y, -0.005f), new Vector2(0, 0),
                        new Vector3(x, y + step, -0.005f), new Vector2(0, 1),
                        new Vector3(x + step, y, -0.005f), new Vector2(1, 0)
                        );

                    factory.AddTriangle(
                        new Vector3(x + step, y, -0.005f), new Vector2(1, 0),
                        new Vector3(x, y + step, -0.005f), new Vector2(0, 1),
                        new Vector3(x + step, y + step, -0.005f), new Vector2(1, 1)
                        );     
                }

                buffer = factory.Create(gdevice);
            }
            this.boundingBox = new BoundingBox(new Vector3(minCorner.X, minCorner.Y, -0.006f), new Vector3(maxCorner.X, maxCorner.Y, -0.005f));
        }

        /// <summary>
        /// Vrátí výšku na souřadnicích.
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public float GetHeigth(Vector2 pos){
            return 0;
        }

        public void Draw(Effect camEffect)
        {
            buffer.Draw(camEffect);
        }
    }
}
