﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace transport.Components
{
    class Quest
    {
        public readonly int Money;
        public readonly int MoneyReserve;
        public readonly string Investor;
        public readonly string Title;
        public readonly string Description;
        public readonly int PassTime;

        public Quest()
        {
            Title = "Nové spojení";
            Description = "Na severu města vyrostlo nové kancelářské \ncentrum. Současné dopravní spojení je ale \nnevhodné, cesta zaměstnanců z centra trvá \npříliš dlouho. Naprojektujte prosím nové, \nrychlejší spojení a vyhovte tak požadavku \nřidičů.";
            Money = 20000;
            MoneyReserve = 1000;
        }
    }
}
