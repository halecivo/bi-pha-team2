﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace transport
{
    public enum ECameraRotations
    {
        eYawLeft, // look left
        eYawRight,
        ePitchUp, // look up
        ePitchDown,
        eRollClockwise, // lean left
        eRollAnticlockwise
    };

    class Camera
    {
        public Vector3 position; // the camera position.
        private Vector3 rotation; // the camera rotation around each axis. 
        private Vector3 up, right, look; // the vectors defining the camera axis.
        public Matrix viewMatrix; // a matrix calculated in the camera.
        public Matrix projectionMatrix;

        public Camera()
        {
            init();
        }

        public void init()
        {
            position = new Vector3(0, 0, 50);
            rotation = new Vector3(0, 0, 0);
            up = new Vector3(0, 1, 0);
            right = new Vector3(1, 0, 0);
            look = new Vector3(0, 0, -1);
            CalculateViewMatrix();
        }

        private void CalculateViewMatrix()
        {



            Vector3 target = position + look;
            viewMatrix = Matrix.CreateLookAt(position, target, up);
        }

        // X axis rotation - also known as Yaw
        private void RotateAroundX(float angle)
        {
            rotation.X += angle;

            // keep the value in the range 0-360 (0 - 2 PI radians)
            if (rotation.X > Math.PI * 2)
                rotation.X -= MathHelper.Pi * 2;
            else if (rotation.Y < 0)
                rotation.X += MathHelper.Pi * 2;

            Matrix pitchMatrix = Matrix.CreateFromAxisAngle(right, rotation.X);

            look = Vector3.Transform(look, pitchMatrix);
            up = Vector3.Transform(up, pitchMatrix);
        }

        // Y axis rotation - also known as Yaw
        private void RotateAroundY(float angle)
        {
            rotation.Y += angle;

            // keep the value in the range 0-360 (0 - 2 PI radians)
            if (rotation.Y > Math.PI * 2)
                rotation.Y -= MathHelper.Pi * 2;
            else if (rotation.Y < 0)
                rotation.Y += MathHelper.Pi * 2;

            Matrix yawMatrix = Matrix.CreateFromAxisAngle(up, rotation.Y);

            look = Vector3.Transform(look, yawMatrix);
            right = Vector3.Transform(right, yawMatrix);
        }

        // Z axis rotation - also known as Yaw
        private void RotateAroundZ(float angle)
        {
            rotation.Z += angle;

            // keep the value in the range 0-360 (0 - 2 PI radians)
            if (rotation.Z > Math.PI * 2)
                rotation.Z -= MathHelper.Pi * 2;
            else if (rotation.Y < 0)
                rotation.Z += MathHelper.Pi * 2;

            Matrix rollMatrix = Matrix.CreateFromAxisAngle(look, rotation.Z);

            up = Vector3.Transform(up, rollMatrix);
            right = Vector3.Transform(right, rollMatrix);
        }

        public void MoveRight(float speed)
        {
            position += right * speed;
            CalculateViewMatrix();
        }

        public void MoveLeft(float speed)
        {
            position -= right * speed;
            CalculateViewMatrix();
        }

        public void MoveForward(float speed)
        {
            position += look * speed;
            CalculateViewMatrix();
        }

        public void MoveBack(float speed)
        {
            position -= look * speed;
            CalculateViewMatrix();
        }

        public void MoveUp(float speed)
        {
            position += up * speed;
            CalculateViewMatrix();
        }

        public void MoveDown(float speed)
        {
            position -= up * speed;
            CalculateViewMatrix();
        }

        public void Rotate(ECameraRotations rot, float angle)
        {
            rotation = Vector3.Zero;
            switch (rot)
            {
                case ECameraRotations.eYawLeft:
                    RotateAroundY(angle);
                    break;
                case ECameraRotations.eYawRight:
                    RotateAroundY(-angle);
                    break;
                case ECameraRotations.ePitchUp:
                    RotateAroundX(angle);
                    break;
                case ECameraRotations.ePitchDown:
                    RotateAroundX(-angle);
                    break;
                case ECameraRotations.eRollClockwise:
                    RotateAroundZ(angle);
                    break;
                default:
                    RotateAroundZ(-angle);
                    break;
                // etc. for the others then :
            }
            CalculateViewMatrix();
        }
    }
}
