﻿using System;
using Microsoft.Xna.Framework;

namespace transport
{
    public struct IDStruct 
    {
        public int id;
        public Vector3 idVector; 
    }

    public static class IDGenerator
    {
        static int counter = 1;
        static int counterName = 1;

        /// <summary>
        /// Vrátí nové volné id.
        /// </summary>
        /// <returns></returns>
        public static IDStruct newID ()
        {
            IDStruct ids = new IDStruct();
            ids.id = counter++;
            ids.idVector = (new Color((ids.id >> 16) & 0xFF, (ids.id >> 8) & 0xFF, ids.id & 0xFF)).ToVector3();

            return ids;
        }

        /// <summary>
        /// Vrátí poslední přiřazené id.
        /// </summary>
        /// <returns></returns>
        public static IDStruct getLastID()
        {
            IDStruct ids = new IDStruct();
            ids.id = counter - 1;
            ids.idVector = (new Color((ids.id >> 16) & 0xFF, (ids.id >> 8) & 0xFF, ids.id & 0xFF)).ToVector3();

            return ids;
        }

        public static string uniqueName()
        {
            return Convert.ToString(counterName++);
        }
    }
}
