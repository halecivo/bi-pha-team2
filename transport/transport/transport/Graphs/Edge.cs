
namespace transport
{
	public class Edge
	{
        public EdgeMetrics cost; //metriky hrany pro vypocty

        public Vertex vertex;
        public string vertex_name;
        public int path_id;

        public Edge (Vertex dest, string vertex_name, int path_id, EdgeMetrics em)
		{  
            this.vertex = dest;
            this.vertex_name = vertex_name;
			this.cost = em;
            this.path_id = path_id;
		}
	}
}
