﻿using System;
using System.Collections.Generic;
using transport.GUI;
using Microsoft.Xna.Framework;

namespace transport
{
    // struktura pro data cesty ke ktere se vaze ukol
    public struct GoalPath
    {
        public string start;
        public string end;

        public GoalPath(string start, string end)
        {
            this.start = start;
            this.end = end;
        }
    }

    // datova struktura pro praci s cestami vseho druhu

    public class Paths
    {
        private Dictionary<string, Vertex> vertices; // uzly a jejich hrany
        // cesty ze vsech uzlu do vsech uzlu - pokud neexistuje spojeni, seznam je prazdny 
        private Dictionary<string, Dictionary<string, List<Vertex>>> paths; 
        // id silnic ze vsech uzlu do vsech uzlu - pokud neexistuje spojeni, mnozina je prazdna
        private Dictionary<string, Dictionary<string, HashSet<int>>> path_ids;

        private GoalPath goalPath; 
        
        private List<CarPath> coloredRoads;
        public ModalWindow mw;

        public Paths(ModalWindow mw, string start, string end)
        {
            vertices = new Dictionary<string, Vertex>();
            paths = new Dictionary<string, Dictionary<string, List<Vertex>>>();
            path_ids = new Dictionary<string, Dictionary<string, HashSet<int>>>();
            goalPath = new GoalPath(start, end);
            coloredRoads = new List<CarPath>();
            this.mw = mw;
        }

        public void printPaths()
        {
            foreach (var v in vertices)
            {
                Console.WriteLine("Vertex " + v.Key);
                Console.Write("\t[");
                foreach (var e in v.Value.edges)
                {
                    Console.Write(e.vertex_name + ", ");
                }
                Console.WriteLine("]");
            }
        }
        
        // prida do grafu novy uzel
        public void AddVertex (Vertex vertex) {
            //Console.WriteLine("Adding new vertex: " + vertex.name);
            this.vertices.Add(vertex.name, vertex);
            this.paths.Add(vertex.name, new Dictionary<string, List<Vertex>>());
            this.path_ids.Add(vertex.name, new Dictionary<string, HashSet<int>>());
        }

        // odebere uzel z datovych struktur
        public void RemoveVertex(Vertex vertex)
        {
            this.vertices.Remove(vertex.name);
            this.paths.Remove(vertex.name);

            List<string> toRemove;

            foreach (var value in paths.Values)
            {
                toRemove = new List<string>();
                foreach (var key in value.Keys)
                {
                    if (key.Equals(vertex.name)) {
                        toRemove.Add(key);
                    }
                }

                foreach (var key in toRemove)
                {
                    value.Remove(key);
                }
            }
        }

        // prida do grafu dve nove hrany - A -> B, B -> A
        public Edge AddEdge (Vertex source, Vertex target, int id, float speed, float time)
        {
            //Console.WriteLine("Adding new edge " + source.name + " -> " + target.name + " to vertex " + source.name);
            Edge e = new Edge(target, target.name, id, new EdgeMetrics(speed, time));
            source.edges.Add(e);
            source.ref_count++;

            return e;
        }

        // zjisti, jestli uz na danych souradnicich neni uzel
        public Vertex VertexExist (float x, float y) {
            Vector2 referencni = new Vector2(x,y);
            Vector2 porovnavaci;
            foreach (var vertex in vertices.Values) {
                //Console.WriteLine(vertex.name + " " + vertex.x + " " + vertex.y);
                porovnavaci = new Vector2(vertex.x, vertex.y);
                if ( (referencni-porovnavaci).Length() < 0.001f ) return vertex;
            }
            
            return null;
        }

        public void AddCarPath(CarPath cp)
        {
            coloredRoads.Add(cp);
        }

        // funkce vrati mnozinu id silnic podle vypocitane nejlepsi cesty
        public HashSet<int> getIdsAlongPath (string start, string end) {
            List<Vertex> path = paths[start][end];

            HashSet<int> set = new HashSet<int>();

            //Console.Write("Vertices alog road: [");
            //foreach (var v in path)
            //{
            //    Console.Write(v.name + ", ");
            //}
            //Console.WriteLine("]");

            Edge e;

            for (int i = 0; i < path.Count - 1; i++) {
                e = path[i].findEdge(path[i + 1].name);
                set.Add(e.path_id);
            }
            
            //Console.Write("After: [");
            //foreach (var v in set)
            //{
            //    Console.Write(v + ", ");
            //}
            //Console.WriteLine("]");
            
            return set;
        }

        // zjisti cenu cesty, zatim jen podle casu
        public float getPathCost(string start, string end)
        {
            float cost = 0f;
            List<Vertex> path = paths[start][end];
            Edge e;

            for (int i = 0; i < path.Count - 1; i++) {
                e = path[i].findEdge(path[i + 1].name);
                cost += e.cost.time;
            }

            return cost;
        }

        // naplni datove struktury
        // mnoziny id silnic podle vypocitane nejlepsi cesty 
        // ze vsech uzlu do vsech uzlu (krome sama sebe)
        public void setAllPathsIDs ()
        {
            foreach (var start in vertices.Values) {
                foreach (var end in vertices.Values) {
                    if (!start.name.Equals(end.name)) {
                        path_ids[start.name][end.name] = getIdsAlongPath(start.name, end.name);
                    }
                }
            }
        }

        // zaktualizuje id silnic pro preddefinovane cesty vozidel
        // podle techto udaju se potom barvi silnice
        public void setAllColoredRoads()
        {
            foreach (var path in coloredRoads)
            {
                path.updateSet(path_ids[path.start][path.end]);
            }
        }

        public Dictionary<string, Vertex> getVertices ()
        {
            return vertices;
        }

        public Dictionary<string, Dictionary<string, List<Vertex>>> getPaths ()
        {
            return paths;
        }

        public List<CarPath> getColoredRoads()
        {
            return coloredRoads;
        }

        public GoalPath getGoalPath()
        {
            return goalPath;
        }
    }
}
