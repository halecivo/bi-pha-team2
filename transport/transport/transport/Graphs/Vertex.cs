﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace transport
{
    public class Vertex
    {
        public string name;
        public float dist;
        public Vertex prev;
        public NodeState state;
        public QueueItem backlink;
        private Paths paths;
        public List<Edge> edges;

        public float x;
        public float y;

        public int ref_count;

        public Vertex(string name, Paths paths, float x = 0, float y = 0)
        {
            this.name = name;
            this.edges = new List<Edge>();
            this.x = x;
            this.y = y;

            this.paths = paths;
            this.ref_count = 0;
        }

        public void init()
        {
            this.dist = Int32.MaxValue;
            this.prev = null;
            this.state = NodeState.fresh;
        }        

        public Edge findEdge (string target) {
            foreach (var e in edges) {
                if (e.vertex.name.Equals(target)) return e;
            }
            
            return null;
        }

        public void remove(List<string> konce)
        {
            // smaze vystupni hrany
            foreach (var konec in konce) {
                Console.WriteLine("Removing edge " + konec + " from vertex " + name);
                removeEdges(konec);
                if (!konec.Equals(name)) {
                    ref_count--;
                }
            }  
            
            if (ref_count == 0) {
                paths.RemoveVertex(this); //smaze sebe z mnoziny uzlu
            }
        }

        // smaze vsechny hrany vedouci do uzlu target
        public void removeEdges(string target)
        {
            List<Edge> toRemove = new List<Edge>();

            foreach (var e in edges) {
                if (e.vertex_name.Equals(target)) {
                    toRemove.Add(e);   
                }
            }

            foreach (var e in toRemove) {
                edges.Remove(e);
            }
        }

        public Vector3 getPos3()
        {
            return new Vector3(x, y, 0);
        }
    }
}
