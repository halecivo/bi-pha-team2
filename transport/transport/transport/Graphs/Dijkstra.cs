using System;
using System.Collections.Generic;

using C5;

namespace transport
{
    class Dijkstra
    {
        IntervalHeap<QueueItem> pq;
        Dictionary<string, Dictionary<string, List<Vertex>>> tmp_paths;
        Dictionary<string, Vertex> vertices;
        Paths paths;
        public bool run;

        public Dijkstra (Paths paths, bool run)
        {
            this.pq = new IntervalHeap<QueueItem>();
            this.paths = paths;
            this.run = run;
        }

        public void calculatePaths(bool debug = false)
        {
            this.tmp_paths = paths.getPaths();
            this.vertices = paths.getVertices();
            
            foreach (var key in this.vertices.Keys)
            {
                dijkstra(key, this.vertices, this.pq);
                if (debug) {
                    Console.WriteLine("Dijkstra for [" + key + "] finished!");
                    foreach (Vertex v in vertices.Values)
                    {
                        Console.WriteLine("=========");
                        Console.WriteLine("Vertex: " + v.name);
                        Console.WriteLine("Distance from start: " + v.dist);
                        if (v.prev != null)
                        {
                            Console.WriteLine("Previous vertex: " + v.prev.name);
                        }
                        else
                        {
                            Console.WriteLine("Vertex is not connected to graph");
                        }
                        Console.WriteLine("Degree of vertex: " + v.ref_count);
                        Console.WriteLine("=========");
                    }
                }
                getAllPaths(key, this.vertices);                
            }
            // aktualizace dat v datovych strukturach pro dalsi algoritmy
            paths.setAllPathsIDs();
            paths.setAllColoredRoads();
            //Console.WriteLine("****************");
            //foreach (var item in tmp_paths.Values)
            //{
            //    foreach (var pp in item.Values)
            //    {
            //        foreach (var ll in pp)
            //        {
            //            Console.WriteLine(ll.name);
            //      }
            //    }
            //}
        }

        public void init (string start, Dictionary<string, Vertex> vertices, IntervalHeap<QueueItem> queue)
        {
            QueueItem item;
            foreach (Vertex v in vertices.Values)
            {
                v.init();
                if (v.name == start) {
                    v.dist = 0;
                }
                
                item = new QueueItem(v);
                item.data.backlink = item;
                item.priority = v.dist;

                pq.Add(ref item.handle, item);
            }

        }

        public void relax (Vertex v, Vertex prev, float cost, IntervalHeap<QueueItem> pq)
        {
            if (v.dist > cost) {
                v.dist = cost;
                v.prev = prev;

                //v.backlink.priority = v.dist;
                // toto nefunguje tak jak bych cekal
                //pq.Replace(v.backlink.handle, v.backlink);

                pq.Delete(v.backlink.handle);

                QueueItem tmp = new QueueItem(v);
                tmp.data.backlink = tmp;
                tmp.priority = v.dist;

                pq.Add(ref tmp.handle, tmp);
            }     
        }

        public void dijkstra (string start, Dictionary<string, Vertex> vertices, IntervalHeap<QueueItem> pq)
        {
            Vertex vert;
            float dst_tmp;

            init(start, vertices, pq);

            while (pq.Count != 0)
            {
                //foreach (Vertex v in vertices.Values)
                //{
                //    Console.WriteLine("=========");
                //    Console.WriteLine("Vertex: " + v.name);
                //    Console.WriteLine("Distance from start: " + v.dist);
                //    Console.WriteLine("Previous vertex: " + v.prev);
                //    Console.WriteLine("=========");
                //}

                vert = pq.DeleteMin().data;

                //Console.WriteLine("Picked vertex " + vert.name);
                foreach (Edge edge in vert.edges)
                {
                    //Console.WriteLine("Edge " + vert.name + " -> " + vertices[edge.vertex].name);
                    dst_tmp = vert.dist + edge.cost.time;
                    if (edge.vertex.state == NodeState.fresh) {
                        edge.vertex.dist = dst_tmp;
                        edge.vertex.prev = vert;

                        //vertices[edge.vertex].backlink.priority = vertices[edge.vertex].dist;
                        
                        pq.Delete(edge.vertex.backlink.handle);

                        QueueItem tmp = new QueueItem(edge.vertex);
                        tmp.data.backlink = tmp;
                        tmp.priority = edge.vertex.dist;

                        pq.Add(ref tmp.handle, tmp);
                        
                        edge.vertex.state = NodeState.open;
                    }
                    else if (edge.vertex.state == NodeState.open) {
                        relax(edge.vertex, vert, dst_tmp, pq);
                    }
                }
                vert.state = NodeState.closed;
                //Console.WriteLine("Closing vertex " + vert.name);
            }
        }

        public List<Vertex> generatePath (string start, string end, Dictionary<string, Vertex> vertices)
        {
                List<Vertex> path = new List<Vertex>();
                Vertex vertex = vertices[end];

                path.Add(vertex);

                while (vertex.prev != null) {
                    path.Add(vertex.prev);
                    vertex = vertex.prev;
                }

                // pokud cesta do uzlu "start" neexistuje, vrat prazdny seznam
                if (path[path.Count - 1].name != start) return new List<Vertex>();

                // cesta se rekonstruje pozpatku, takze potrebujeme pretocit cestu
                path.Reverse();
                return path;
        }

        public void getAllPaths(string key, Dictionary<string, Vertex> vertices) {
            foreach (var vertex in vertices.Values) {
                //Console.WriteLine("Processing path " + key + " -> " + vertex.name);
                if (!vertex.name.Equals(key)) {
                    tmp_paths[key][vertex.name] = generatePath(key, vertex.name, vertices);
                }
            }
        }
    }
}
