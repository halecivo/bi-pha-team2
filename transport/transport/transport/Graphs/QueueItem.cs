﻿using System;

using C5;

namespace transport
{
    public class QueueItem : IComparable<QueueItem>
    {
        public Vertex data;
        public float priority;

        public IPriorityQueueHandle<QueueItem> handle;
        
        public QueueItem(Vertex data)
        {
            this.data = data;
            this.handle = null;
        }

        public int CompareTo(QueueItem that)
        {
            return this.priority.CompareTo(that.priority);
        }
    }
}
