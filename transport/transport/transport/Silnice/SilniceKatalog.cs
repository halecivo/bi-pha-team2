﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace transport
{
    /// <summary>
    /// Katalog všech postavených silnic ve hře.
    /// </summary>
    class SilniceKatalog
    {
        List<Silnice> silnice = new List<Silnice>();
        GraphicsDevice gdevice;

        private Vector3 findS(KonecSilnice Ak, Vector3 B)
        {
            Vector3 nA = Ak.getDir();
            Vector3 A = Ak.getPosition();
            Vector3 O = (B + A)/2;

            Vector3 S = Vector3.Zero;

            float kO = 0;
            float kA = 0;

            if ((B.Y - O.Y) != 0)
            {
                kO = (O.X - B.X) / (B.Y - O.Y);
            }

            if (nA.Y != 0)
            {
                kA = nA.X / nA.Y;
            }

            if ((B.Y - O.Y) == 0)
            {
                if (nA.X == 0)
                {
                    return A;
                }
                else if (nA.Y == 0)
                {
                    return O;
                }
                else
                {
                    S.X = O.X;
                    S.Y = (S.X - A.X) * kA + A.Y;
                    return S;

                }
            }
            else if (nA.Y == 0)
            {
                if (A.X == O.X)
                {
                    return A;
                }
                S.X = A.X;
                S.Y = (S.X - O.X) * kO + O.Y;
                return S;
            }
            else
            {
                if (kO == kA)
                {
                    return A;
                }
                else
                {
                    //S.X = (A.X * kA - O.X * kO - A.Y + O.Y) / (kA - kO);
                    //S.Y = (S.X - A.X) * kA + A.Y;
                    S = A + nA * (A.Y - A.X / kO - O.Y + O.X / kO) / (kA / kO - 1);
                    return S;
                }
            }
        }

        public SilniceKatalog(GraphicsDevice gdevice)
        {
            this.gdevice = gdevice;
        }

        public void BuildRoad(Vector3 A, Vector3 B, Vector3 S, ObloukBuilder type)
        {
            silnice.Add(type.Create(A, B, S, gdevice));
            silnice[silnice.Count - 1].setID(IDGenerator.newID());
        }

        public void BuildRoad(Vector3 A, Vector3 B, ObloukBuilder type)
        {
            silnice.Add(type.Create(A, B, gdevice));
            silnice[silnice.Count - 1].setID(IDGenerator.newID());
        }

        public void BuildRoad(KonecSilnice A, Vector3 B, ObloukBuilder type)
        {
            Vector3 S = findS(A, B);
            if (S == A.getPosition())
            {
                BuildRoad(A.getPosition(), B, type);
            }
            else
            {
                System.Console.WriteLine(A.getPosition() + " " + B + " " + S);
                BuildRoad(A.getPosition(), B, S, type);
            }
        }

        public void DestroyRoad(int id)
        {
            silnice.RemoveAt(id-1);
            //for (int i = (id-1); i < silnice.Count; i++):w
            //{
            //    silnice[i].setID(i+1);
            //}
        }

        public Silnice get(int id)
        {
            return silnice[id];
        }

        //public void PixelPickDraw(BasicEffect effect)
        //{
        //    for (int i = 0; i < silnice.Count; i++)
        //    {
        //        effect.DiffuseColor = silnice[i].getVectorID();               
        //        silnice[i].Draw(effect);
        //    }
        //}

        public void Draw(Effect effect)
        {
            foreach (Silnice s in silnice)
            {
                effect.Parameters["pickColor"].SetValue(s.getVectorID());
                s.Draw(effect);
            }
        }

        internal int getVectorID()
        {
            throw new System.NotImplementedException();
        }
    }
}
