﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace transport
{
    public class GeometricBuffer<VertexType> : IDisposable where
    VertexType : struct
    {
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        private int primitiveCount;
        private int verticesCount;
        public VertexType[] VertexArray;

        //public bool IsTextureTransparent = false;
        //public bool IsAdditive = false;
        //public bool AntiAliasTexture = true;
        //bool rasterizerIsDirty = true;
        //bool wireFrame = false;
        //public bool Wireframe
        //{
        //    get
        //    {
        //        return wireFrame;
        //    }
        //    set
        //    {
        //        if (wireFrame == value)
        //        {
        //            return;
        //        }
        //        wireFrame = value;
        //        rasterizerIsDirty = true;
        //    }
        //}
        //bool cull = true;
        //public bool Cull
        //{
        //    get
        //    {
        //        return cull;
        //    }

        //    set
        //    {
        //        if (cull == value)
        //        {
        //            return;
        //        }
        //        cull = value;
        //        rasterizerIsDirty = true;
        //    }
        //}


        public GeometricBuffer(
            VertexBuffer vertexBuffer,
            int verticesCount,
            IndexBuffer indexBuffer,
            int primitiveCount,
            VertexType[] vertexArray)
        {
            this.vertexBuffer = vertexBuffer;
            this.indexBuffer = indexBuffer;
            this.primitiveCount = primitiveCount;
            this.verticesCount = verticesCount;
            this.VertexArray = vertexArray;
            if (this.vertexBuffer is DynamicVertexBuffer)
            {
                ((DynamicVertexBuffer)this.vertexBuffer).ContentLost += new EventHandler<EventArgs>(GeometricBuffer_ContentLost);
            }
        }

        void GeometricBuffer_ContentLost(
                 object sender, EventArgs e)
        {
            UpdateVertices();
        }
        public void UpdateVertices()
        {
            vertexBuffer.SetData<VertexType>(VertexArray);
        }

        ~GeometricBuffer()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (vertexBuffer != null)
                {
                    vertexBuffer.Dispose();
                }
                if (indexBuffer != null)
                {
                    indexBuffer.Dispose();
                }
            }
        }

        public void Draw(Effect effect)
        {
            var graphicsDevice = effect.GraphicsDevice;
            //if (IsAdditive)
            //{
            //    graphicsDevice.BlendState = BlendState.Additive;
            //    graphicsDevice.DepthStencilState =
            //                       DepthStencilState.DepthRead;
            //}
            //else if (IsTextureTransparent)
            //{
            //    graphicsDevice.BlendState = BlendState.AlphaBlend;
            //    graphicsDevice.DepthStencilState =
            //                       DepthStencilState.DepthRead;
            //}
            //else
            //{
            //    graphicsDevice.BlendState = BlendState.Opaque;
            //    graphicsDevice.DepthStencilState =
            //                       DepthStencilState.Default;
            //}
            //if (AntiAliasTexture)
            //{
            //    graphicsDevice.SamplerStates[0] =
            //                       SamplerState.LinearWrap;
            //}
            //else
            //{
            //    graphicsDevice.SamplerStates[0] =
            //                       SamplerState.PointWrap;
            //}
            //if (rasterizerIsDirty)
            //{
            //    graphicsDevice.RasterizerState = new RasterizerState()
            //    {
            //        CullMode = cull ?
            //  CullMode.CullCounterClockwiseFace : CullMode.None,
            //        FillMode = wireFrame ?
            //  FillMode.WireFrame : FillMode.Solid
            //    };
            //    rasterizerIsDirty = false;
            //}
            graphicsDevice.SetVertexBuffer(vertexBuffer);
            graphicsDevice.Indices = indexBuffer;
            foreach (EffectPass effectPass in
                      effect.CurrentTechnique.Passes)
            {
                effectPass.Apply();
                graphicsDevice.DrawIndexedPrimitives(
                    PrimitiveType.TriangleList,
                    0, 0, verticesCount, 0, primitiveCount);
            }
        }



    }
}
