﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace transport
{


    class VertexPositionNormalTextureGeometricBufferFactory :
GeometricBufferFactory<VertexPositionNormalTexture>
    {

        public void AddTriangle(
                        Vector3 vertex1,
                        Vector2 textureCoordinate1,
                        Vector3 normal1,
                        Vector3 vertex2,
                        Vector2 textureCoordinate2,
                        Vector3 normal2,
                        Vector3 vertex3,
                        Vector2 textureCoordinate3,
                        Vector3 normal3
            )
        {
            AddTriangle(
                new VertexPositionNormalTexture(
                        vertex1, normal1, textureCoordinate1),
                new VertexPositionNormalTexture(
                        vertex2, normal2, textureCoordinate2),
                new VertexPositionNormalTexture(
                        vertex3, normal3, textureCoordinate3));
        }

        public void AddTriangle(
            Vector3 vertex1, Vector2 textureCoordinate1,
            Vector3 vertex2, Vector2 textureCoordinate2,
            Vector3 vertex3, Vector2 textureCoordinate3)
        {

            // seřazení bodů protisměru hodinových ručiček
            if (Vector3.Cross(vertex3 - vertex1, vertex2 - vertex1).Z < 0)
            {
                vertex1 += vertex2;
                vertex2 = vertex1 - vertex2;
                vertex1 = vertex1 - vertex2;

                textureCoordinate1 += textureCoordinate2;
                textureCoordinate2 = textureCoordinate1 - textureCoordinate2;
                textureCoordinate1 = textureCoordinate1 - textureCoordinate2;
            }

            var planar1 = vertex2 - vertex1;
            var planar2 = vertex1 - vertex3;
            var normal = Vector3.Normalize(
                             Vector3.Cross(planar1, planar2));
            AddTriangle(
                new VertexPositionNormalTexture(
                        vertex1, normal, textureCoordinate1),
                new VertexPositionNormalTexture(
                        vertex2, normal, textureCoordinate2),
                new VertexPositionNormalTexture(
                        vertex3, normal, textureCoordinate3));
        }

        public void AddTriangle(
            VertexPositionNormalTexture vertex1,
            VertexPositionNormalTexture vertex2,
            VertexPositionNormalTexture vertex3)
        {
            vertices.Add(vertex1);
            vertices.Add(vertex2);
            vertices.Add(vertex3);
        }
        public void AddPane(
            Vector3 topLeft,
            Vector2 topLeftTexture,
            Vector3 topLeftNormal,
            Vector3 topRight,
            Vector2 topRightTexture,
            Vector3 topRightNormal,
            Vector3 bottomRight,
            Vector2 bottomRightTexture,
            Vector3 bottomRightNormal,
            Vector3 bottomLeft,
            Vector2 bottomLeftTexture,
            Vector3 bottomLeftNormal
    )
        {
            AddTriangle(
                topLeft, topLeftTexture, topLeftNormal,
                topRight, topRightTexture, topRightNormal,
                bottomRight, bottomRightTexture, bottomRightNormal);

            AddTriangle(
                bottomRight, bottomRightTexture, bottomRightNormal,
                bottomLeft, bottomLeftTexture, bottomLeftNormal,
                topLeft, topLeftTexture, topLeftNormal);
        }
        public void AddPane(
            Vector3 topLeft, Vector2 topLeftTexture,
            Vector3 topRight, Vector2 topRightTexture,
            Vector3 bottomRight, Vector2 bottomRightTexture,
            Vector3 bottomLeft, Vector2 bottomLeftTexture
            )
        {
            AddTriangle(
                topLeft, topLeftTexture,
                topRight, topRightTexture,
                bottomRight, bottomRightTexture);
            AddTriangle(
                bottomRight, bottomRightTexture,
                bottomLeft, bottomLeftTexture,
                topLeft, topLeftTexture);
        }

    }
}
