﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace transport
{
    public class GeometricBufferFactory<VertexType> where VertexType : struct
    {
        protected List<VertexType> vertices =
            new List<VertexType>();
    

        private void CalculateGeometry(out List<VertexType> uniqueVertices, out List<ushort> indices)
        {
            uniqueVertices = new List<VertexType>();
            indices = new List<ushort>();
            var indexByVertex = new Dictionary<VertexType, ushort>();
            var indexTally = (ushort)0;
            foreach (var vertex in vertices)
            {
                ushort index;
                if (!indexByVertex.TryGetValue(vertex, out index))
                {
                    index = indexTally;
                    indexByVertex[vertex] = index;
                    uniqueVertices.Add(vertex);
                    indexTally++;
                }
                indices.Add(index);
            }
        }
        public GeometricBuffer<VertexType> Create(
                   GraphicsDevice device,
                   bool dynamicVertexBuffer = false)
        {
            List<VertexType> uniqueVertices;
            List<ushort> indices;
            CalculateGeometry(out uniqueVertices, out indices);
            var vertexArray = uniqueVertices.ToArray();
            var indexArray = indices.ToArray();
            VertexBuffer vertexBuffer;
            if (dynamicVertexBuffer)
            {
                vertexBuffer = new DynamicVertexBuffer(device,
                                       typeof(VertexType),
                                       uniqueVertices.Count,
                                       BufferUsage.WriteOnly);
            }
            else
            {
                vertexBuffer = new VertexBuffer(device,
                                       typeof(VertexType),
                                       uniqueVertices.Count,
                                       BufferUsage.None);
            }
            vertexBuffer.SetData(vertexArray);
            var indexBuffer = new IndexBuffer(
                                      device,
                                      typeof(ushort),
                                      indices.Count,
                                      BufferUsage.None);
            indexBuffer.SetData(indexArray);
            return new GeometricBuffer<VertexType>(
                vertexBuffer,
                uniqueVertices.Count,
                indexBuffer,
                indices.Count / 3,
                vertexArray);
        }
        
    }




}
