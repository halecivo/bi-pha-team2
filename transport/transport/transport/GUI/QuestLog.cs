﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using TomShane.Neoforce.Controls;

namespace transport.GUI
{
    class QuestLog
    {
        public float PassTime;
        public int Money;

        public QuestLog(){
            Money = 0;
            PassTime = 0;
        }

        public void SpendMoney(int value)
        {
            Money += value;
        }

        public void UpdatePassTime(float newTime)
        {
            PassTime = newTime;
        }

        public float getPassTime()
        {
            return PassTime;
        }
    }
}
