﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace transport.GUI
{
    public class Flag
    {
        public Vector3 pos;
        public FlagType type;

        public Flag(Vector3 pos, FlagType type)
        {
            this.pos = pos;
            this.type = type;
        }

        // x,y pozice se urci v Add metode FlagList podle souradnic uzlu
        public Flag(FlagType type)
        {
            this.type = type;
        }

        public void Draw (GraphicsDevice graphics, Effect effect) {
            GeometricBuffer<VertexPositionNormalTexture> buffer;
            VertexPositionNormalTextureGeometricBufferFactory factory = new VertexPositionNormalTextureGeometricBufferFactory();

            int step = 10;

            Vector3 A = new Vector3(this.pos.X, this.pos.Y, 0);
            Vector3 B = new Vector3(this.pos.X + step, this.pos.Y, 0);
            Vector3 C = new Vector3(this.pos.X, this.pos.Y + step, 0);
            Vector3 D = new Vector3(this.pos.X + step, this.pos.Y + step, 0);

            factory.AddPane(
                A, new Vector2(0, 1), new Vector3(0, 0, 1),
                B, new Vector2(1, 1), new Vector3(0, 0, 1),
                C, new Vector2(0, 0), new Vector3(0, 0, 1),
                D, new Vector2(1, 0), new Vector3(0, 0, 1)
                );

            buffer = factory.Create(graphics);
            buffer.Draw(effect);
        }
    }
}
