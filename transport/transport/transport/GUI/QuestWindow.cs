﻿using System.Globalization;
using transport.Components;
using TomShane.Neoforce.Controls;

namespace transport.GUI
{
    class QuestWindow : Window
    {
        private ProgressBar moneyBar;
        private int availibleMoney, reserveMoney, requiredTime = 1800;
        private Label moneyLabel, money, title, description;
        private int score = 0;

        private Label passTimeLabel, passTime, maxSpeedLabel, maxSpeed, pathLength, pathLenghLabel, requirementsLabel,
            reqs;
        private ImageBox image;
        public CheckBox markPoints;
        private Button closeButton, completeButton;

        public QuestWindow(Manager manager, Quest quest)
            : base(manager)
        {
            Text = "Zadání investora";
            CloseButtonVisible = false;
            Height = 300;
            Width = 400;
            Center();

            #region info

            title = new Label(manager);
            title.SetPosition(
                8, 8);
            title.Width = 200;
            title.Text = quest.Title;
            title.Parent = this;

            image = new ImageBox(manager);
            image.Width = 80;
            image.Height = 100;
            image.Left = 8;
            image.Top = title.Top + title.Height + 8;
            image.Parent = this;

            description = new Label(manager);
            description.SetPosition(16 + image.Width , 16 + title.Height);
            description.Text = quest.Description;
            description.Width = this.ClientWidth - 24 - image.Width;
            description.Height = 100;
            description.Alignment = Alignment.TopLeft;
            description.Parent = this;

            availibleMoney = quest.Money;
            reserveMoney = quest.MoneyReserve;

            moneyBar = new ProgressBar(manager);
            moneyBar.Range = quest.Money;
            moneyBar.Value = quest.Money;
            //moneyBar.Top = description.Top + description.Height + 8;
            moneyBar.Top = title.Top;
            moneyBar.Left = ClientWidth - moneyBar.Width - 8;
            moneyBar.Parent = this;

            ToolTip moneyBarToolTip = new ToolTip(manager);
            moneyBar.ToolTip.Text = string.Format(new CultureInfo("cs-CZ"), "{0:c}",quest.Money);

            moneyLabel = new Label(manager);
            moneyLabel.Text = "Rozpočet ";
            moneyLabel.Width = 65;
            moneyLabel.Top = moneyBar.Top;
            moneyLabel.Left = moneyBar.Left - moneyLabel.Width - 8;
            moneyLabel.Parent = this;

            #endregion

            passTime = new Label(manager);
            passTime.Top = description.Top + description.Height + 8;
            passTime.Left = ClientWidth - passTime.Width - 8;
            passTime.Text = "00:08:42";
            passTime.Parent = this;
            passTime.Alignment = Alignment.MiddleRight;

            passTimeLabel = new Label(manager);
            passTimeLabel.Top = passTime.Top;
            passTimeLabel.Width = 75;
            passTimeLabel.Left = passTime.Left - passTimeLabel.Width - 8;
            passTimeLabel.Text = "Jízdní doba";
            passTimeLabel.Parent = this;

            maxSpeed = new Label(manager);
            maxSpeed.Top = passTime.Top + passTime.Height;
            maxSpeed.Left = passTime.Left;
            maxSpeed.Text = "N/A";
            maxSpeed.Alignment = Alignment.MiddleRight;
            maxSpeed.Parent = this;

            maxSpeedLabel = new Label(manager);
            maxSpeedLabel.Top = maxSpeed.Top;
            maxSpeedLabel.Left = passTimeLabel.Left;
            maxSpeedLabel.Text = "Rychlost";
            maxSpeedLabel.Parent = this;

            requirementsLabel = new Label(manager);
            requirementsLabel.Top = description.Top + description.Height + 8;
            requirementsLabel.Left = 8;
            requirementsLabel.Text = "Požadavky";
            requirementsLabel.Parent = this;

            closeButton = new Button(manager);
            closeButton.Top = ClientHeight - closeButton.Height - 8;
            closeButton.Left = ClientWidth - closeButton.Width - 8;
            closeButton.Text = "Zavřít";
            closeButton.Click += new EventHandler(closeButton_Click);
            closeButton.Parent = this;

            markPoints = new CheckBox(manager);
            markPoints.Text = "zobrazit cíle na mapě";
            markPoints.Width = 150;
            markPoints.Checked = true;
            markPoints.Top = ClientHeight - markPoints.Height - 8;
            markPoints.Left = 8;
            markPoints.Parent = this;

            reqs = new Label(manager);
            reqs.Top = requirementsLabel.Top + requirementsLabel.Height;
            reqs.Left = requirementsLabel.Left + 4;
            reqs.Width = 150;
            reqs.Height = ClientHeight - reqs.Top - (ClientHeight - markPoints.Top);
            reqs.Alignment = Alignment.TopLeft;
            reqs.Text = "- rozpočet " + (quest.Money) + " ml. Kč\n- průjezd 6 minut";
            reqs.Parent = this;

            completeButton = new Button(manager);
            completeButton.Width = ClientWidth - 8 - passTimeLabel.Left;
            completeButton.Top = closeButton.Top - completeButton.Height - 8;
            completeButton.Left = passTimeLabel.Left;
            completeButton.Text = "Dokončit úkol";
            completeButton.Click += new EventHandler(completeButton_Click);
            completeButton.Enabled = false;
            completeButton.Parent = this;

            this.Hide();
            manager.Add(this);
        }

        void completeButton_Click(object sender, EventArgs e)
        {
            ModalWindow md = new ModalWindow(this.Manager, this.Left + this.Width / 2 - 125, this.Top + this.Height / 2 - 50);
            md.ShowModal("Úkol dokončen", "Gratuluji, získali jste " + score + " bodů.");
            if (md.ModalResult == ModalResult.Ok)
            {
                ExitDialog exit = new ExitDialog(this.Manager);
                exit.Show();
            }
            
        }

        public void Update(QuestLog qlog)
        {
            if (qlog.Money < availibleMoney)
            {
                moneyBar.Range = availibleMoney;
                moneyBar.Value = availibleMoney - qlog.Money;
            }
            else
            {
                moneyBar.Range = reserveMoney;
                moneyBar.Color = Microsoft.Xna.Framework.Color.Red;
                moneyBar.Value = availibleMoney + reserveMoney - qlog.Money;
            }
            
            moneyBar.ToolTip.Text = string.Format(new CultureInfo("cs-CZ"), "{0:c}", moneyBar.Value*1000);
            
            int timeSec = (int)(qlog.PassTime * 1000);

            score = (availibleMoney - qlog.Money) * 10 + requiredTime - timeSec * 5;

            if (timeSec > 0)
            {
                passTime.Text = (timeSec / 60).ToString() + "m " + (timeSec % 60).ToString() + "s";
            }
            else
            {
                passTime.Text = "N/A";
            }

            if (timeSec > 0 && timeSec < requiredTime && qlog.Money < availibleMoney)
            {
                completeButton.Enabled = true;
            }
            else
            {
                completeButton.Enabled = false;
            }
        }

        void closeButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
