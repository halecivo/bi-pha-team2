﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace transport.GUI
{
    public enum FlagType
    {
        start,
        middle,
        end
    }

    class FlagList
    {
        List<Flag> list;
        Texture2D start_tex;
        Texture2D end_tex;
        Paths paths;

        GraphicsDevice graphics;

        public FlagList (Texture2D start_tex, Texture2D end_tex, GraphicsDevice graphics, Paths paths)
        {
            this.list = new List<Flag>();
            this.start_tex = start_tex;
            this.end_tex = end_tex;
            this.paths = paths;
            this.graphics = graphics;
        }

        public void Add(Flag f, string vertex = null)
        {
            list.Add(f);
            if (vertex != null) {
                Dictionary<string, Vertex> d = paths.getVertices();
                f.pos = d[vertex].getPos3();
            }
        }

        public void Draw (Effect effect, RTarget target)
        {
            if (target == RTarget.Regular)
            {
                foreach (var f in list)
                {
                    switch (f.type)
                    {
                        case FlagType.start:
                            effect.Parameters["xTexture"].SetValue(start_tex);
                            break;
                        case FlagType.end:
                            effect.Parameters["xTexture"].SetValue(end_tex);
                            break;
                        //default:
                        //    effect.Parameters["xTexture"].SetValue(middle_tex);
                    }
                    f.Draw(graphics, effect);
                }
            }
        }
    }
}
