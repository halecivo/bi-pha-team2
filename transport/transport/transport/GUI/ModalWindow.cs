﻿using TomShane.Neoforce.Controls;

namespace transport.GUI
{
    public enum ModalType
    {

    }

    public class ModalWindow : Window
    {
        Button bOK;
        Label message;
        int defaultLeft;
        int defaultTop;
        //static ModalWindow staticModal;

        public ModalWindow(Manager manager, int left, int top) : base(manager)
        {
            this.Width = 250;
            this.Height = 100;
            this.SetPosition(left - this.Width/2, top - this.Height/2);
            this.Resizable = false;
            this.CloseButtonVisible = false;
            this.Text = "Transport zpráva";
            this.StayOnTop = true;

            message = new Label(manager);
            message.Top = 8;
            message.Width = this.ClientWidth - 16;
            message.Left = 8;
            message.Alignment = Alignment.TopCenter;
            message.Parent = this;

            bOK = new Button(manager);
            bOK.Text = "OK";
            bOK.Height = 24;
            bOK.Width = 74;
            bOK.Left = (this.ClientWidth / 2) - (bOK.Width / 2);
            bOK.Top = this.ClientHeight - bOK.Height - 8;
            bOK.Anchor = Anchors.Bottom;
            bOK.Visible = true;
            bOK.ModalResult = ModalResult.Ok;
            bOK.Click += new EventHandler(bOK_Click);
            bOK.Parent = this;

            this.Hide();
            manager.Add(this);

            //staticModal = this;

        }

        public void ShowModal(string title, string text, int left = -1, int top = -1)
        {
            if (left < 0)
            {
                left = defaultLeft;
            }

            if (top < 0)
            {
                top = defaultTop;
            }

            //staticModal.Text = title;
            this.Text = title;
            message.Text = text;

            //staticModal.Resizable = resize;
            //this.Resizable = resize;
            //staticModal.ShowModal();
            this.Show();
        }

        void bOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
