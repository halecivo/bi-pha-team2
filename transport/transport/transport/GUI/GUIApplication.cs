﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TomShane.Neoforce.Controls;
using transport.Objects;
using transport.GUI;
using IniParser;

namespace transport
{

    public enum RTarget {
        Regular,
        Back,
        Collision
    };

    /// <summary>
	/// The GUI application class your application will be built upon.
	/// </summary>
	public class GUIApplication : Application
	{

        #region Attributes

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D terrainTexture;
        Texture2D roadTexture;
        Texture2D flagStartTexture;
        Texture2D flagEndTexture;

        FlagList flagList;

        Effect shader;
        bool useSimpleRoadConnector = true;

        // render targety
        RenderTarget2D regularRT;
        RenderTarget2D backRT;
        RenderTarget2D collisionRT;

        //velikost backBufferu
        int window_width;
        int window_height;

        // myš
        MouseState mouseState;
        MouseState mouseStatePrevious;
        bool inverzniPosun = false; // inverzní posun = tažením "posouvám mapu po stole"

        Rectangle mousePixel;

        //klávesnice
        KeyboardState keyState;
        KeyboardState keyStatePrevious;

        // aktivni nastroj
        ActiveTool activeTool;

        transport.GUI.Tools selectedTool;
        ObloukBuilder selectedRoadProfile = SilniceType.S60_40;
        KrizovatkaType selectedCrossType = KrizovatkaType.Kriz;
        QuestLog questLog = new QuestLog();

        // zvuky
        //private Song song;

        // kamera
        Camera camera;
        float zoomStep = 2.5f;
        const float viewAngle = 70.0f;
        Vector3 kameraPos = new Vector3(0, 0, 1);
        Vector3 kameraTarget = Vector3.Zero;
        float angleX = 0;
        float angleY = 0;
        float angleZ = 0;
        float windowAspectRatio;
        Matrix projectionMatrix;

        //budova
        BuildingKatalog budovy;
        float aspectRatio;


        // teren
        TerrainGenerator terrain;

        // tlacitka pro panel nastroju
        ArrayList button_list;
        string[] actions;
        int icon_size;
        int button_cnt;

        IniParser.FileIniDataParser parser;
        IniData config;
        IniData quests;

        // start a cil cesty v ukolu
        string quest_path_start;
        string quest_path_end;

        List<string> car_paths_str;
        bool showRoadLoad; 

        SilniceKatalog silnice;

        // datova struktura pro vyhledavani cest
        Paths paths;
        Dijkstra dijkstra;
        bool dijkstra_run = false; // zablokuje pocitani dijstry dokud se nevyrobi cely graf
        bool dijkstra_debug; 

        //click on ground
        BoundingSphere bullet = new BoundingSphere(Vector3.Zero, 0.05f);
        #endregion

        #region NeoForce prvky
        Manager manager;
        private QuestWindow WQuest;
        ModalWindow mw;
        ModalWindow pathWindow;   
        #endregion

        #region Constructors
        /// <summary>
		/// Creates an application using the Default skin file. 
		/// </summary>
		public GUIApplication()
			: base("Default")
		{
            graphics = this.Graphics;

			Content.RootDirectory = "Content";
            manager = new Manager(this, graphics, "Default");

			manager.SkinDirectory = "Content/Skins";
			manager.LayoutDirectory = "Content/Layouts";
			ExitConfirmation = true;


            parser = new FileIniDataParser();
            config = parser.LoadFile("config.ini");
            quests = parser.LoadFile("quests.ini");

            car_paths_str = new List<string>();
            showRoadLoad = false;
                        
            dijkstra_debug = Convert.ToBoolean(config["Dijkstra"]["debug"]);

            this.Window.Title = config["Window"]["title"];
            //this.Window.AllowUserResizing = true;

            window_width = Convert.ToInt32(config["Window"]["width"]);
            window_height = Convert.ToInt32(config["Window"]["height"]);

            graphics.PreferredBackBufferWidth = window_width;
            graphics.PreferredBackBufferHeight = window_height;
            graphics.PreferMultiSampling = true;
            graphics.IsFullScreen = false;
            Window.AllowUserResizing = false;
            this.IsMouseVisible = true;
		}
		#endregion

		#region Initialize
		/// <summary>
		/// Initializes the application.
		/// </summary>
		protected override void Initialize()
        {
            manager.SetSkin("Default");
            manager.Initialize();

            // Create and assign render target for UI rendering.
            manager.RenderTarget = new RenderTarget2D(GraphicsDevice, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight, false, SurfaceFormat.Color, DepthFormat.None, 0, RenderTargetUsage.DiscardContents);
            // Maximum of frames we want to render per second (applies only for Neoforce, not Game itself)
            manager.TargetFrames = 60; 
            
            WQuest = new QuestWindow(manager, new Components.Quest());
            // inicializace ModalWindow (trochu divná, vím)
            //new ModalWindow(manager, window_width / 2, window_height / 2);

            // inicializace oken
            mw = new ModalWindow(manager, window_width / 2, window_height / 2);
            pathWindow = new ModalWindow(manager, window_width / 2, window_height / 2);
            
            quest_path_start = Convert.ToString(quests["Quest1Goal"]["start"]);
            quest_path_end = Convert.ToString(quests["Quest1Goal"]["end"]);
            
            paths = new Paths(pathWindow, quest_path_start, quest_path_end);
            
            button_list = new ArrayList();
            actions = config["Icons"]["list"].Split(',');
            button_cnt = actions.Length;

            this.activeTool = new ActiveTool();

            icon_size = System.Convert.ToInt32(config["Icons"]["size"]);

            // konektor silnic
            int usesimple = System.Convert.ToInt32(config["Global"]["useSimpleRoadConnector"]);
            if (usesimple == 1)
            {
                useSimpleRoadConnector = true;
            }
            else
            {
                useSimpleRoadConnector = false;
            }
            
            // trasy aut            
            for (int i = 0; i < Convert.ToInt32(quests["Quest1Paths"]["count"]); i++) {
                car_paths_str.Add(quests["Quest1Paths"]["path" + Convert.ToString(i + 1)]);
            }

            string[] arr = null;
            string tmp, tmp2;
            string start, end;
            int count;
            foreach (var sp in car_paths_str) {
                tmp = sp;
                count = Convert.ToInt32(tmp.Split(':')[1]);
                tmp2 = tmp.Split(':')[0];
                arr = tmp2.Split(',');

                start = arr[0];
                end = arr[1];

                paths.AddCarPath(new CarPath(start, end, count));
            }  

            camera = new Camera();
            this.windowAspectRatio = (float)GraphicsDevice.Viewport.Width / (float)GraphicsDevice.Viewport.Height;
            this.projectionMatrix = Matrix.CreatePerspectiveFieldOfView( //vychozí nastaveni kamery pro vsechny objekty
                MathHelper.ToRadians(viewAngle),  // 70 degree angle
                windowAspectRatio,
                1.0f, 1000.0f);
            camera.projectionMatrix = projectionMatrix;

            regularRT = new RenderTarget2D(graphics.GraphicsDevice, window_width, window_height);
            backRT = new RenderTarget2D(graphics.GraphicsDevice, window_width, window_height);
            collisionRT = new RenderTarget2D(graphics.GraphicsDevice, window_width, window_height);

            Utils.MouseController.SetGDevice(graphics.GraphicsDevice);

            this.keyState = Keyboard.GetState();
            this.keyStatePrevious = keyState;
   
            dijkstra = new Dijkstra(paths, dijkstra_run);
			base.Initialize();


//            TomShane.Neoforce.Controls.Label
		}

		#endregion

		#region Load Content
		/// <summary>
		/// Loads content.
		/// </summary>
		protected override void LoadContent()
		{
            spriteBatch = new SpriteBatch(GraphicsDevice);

            int xpos = 0;
            int id;
            foreach (string s in actions)
            {
                switch (s.Trim())
                {
                    case "exit":
                        id = 0;
                        break;
                    case "save":
                        id = 1;
                        break;
                    case "load":
                        id = 2;
                        break;
                    case "buldozer":
                        id = 3;
                        break;
                    case "cross":
                        id = 4;
                        break;
                    case "road":
                        id = 5;
                        break;
                    case "quest":
                        id = 6;
                        break;
                    case "setting":
                        id = 7;
                        break;
                    default:
                        continue;
                }
                button_list.Add(new TButton(id, "tlacitko_" + s.Trim() + icon_size + "_default", "tlacitko_" + s.Trim() + icon_size + "_over", "tlacitko_" + s.Trim() + icon_size + "_click", xpos, 0));
                xpos += icon_size + 2;
            }

            foreach (TButton b in button_list)
            {
                b.texture = Content.Load<Texture2D>("Content\\Buttons\\" + b.texture_file);
                b.texture_hover = Content.Load<Texture2D>("Content\\Buttons\\" + b.texture_file_hover);
                b.texture_click = Content.Load<Texture2D>("Content\\Buttons\\" + b.texture_file_click);
            }

            terrainTexture = Content.Load<Texture2D>("Content\\Textures\\grass");
            roadTexture = Content.Load<Texture2D>("Content\\Textures\\tex_road64");

            flagStartTexture = Content.Load<Texture2D>("Content\\Textures\\Flags\\start");
            flagEndTexture = Content.Load<Texture2D>("Content\\Textures\\Flags\\end");

            flagList = new FlagList(flagStartTexture, flagEndTexture, graphics.GraphicsDevice, paths);

            silnice = new SilniceKatalog(GraphicsDevice, paths, showRoadLoad, dijkstra, questLog, WQuest);

            // silnice test
            silnice.BuildProjectedRoad(
                new Vector3(-20f, 0, 0),
                new Vector3(0, 20, 0),
                new Vector3(-1, 0, 0),
                new Vector3(0, 0, 0),
                selectedRoadProfile
            );

            silnice.BuildRoad(
                silnice.get(1).getKonec(new Vector3(8f, 12f, 0)),
                new Vector3(8f, 12f, 0),
                selectedRoadProfile
                );


            silnice.BuildRoad(
                silnice.get(2).getKonec(new Vector3(8f, -4f, 0)),
                new Vector3(8f, -4f, 0),
                selectedRoadProfile
                );

            silnice.BuildCross(
                silnice.get(3).getKonec(new Vector3(8, -4, 0)),
                KrizovatkaType.T,
                1
                );

            silnice.BuildCross(
                silnice.get(4).getKonec(new Vector3(23, -19, 0)),
                KrizovatkaType.Kriz
            );

            silnice.BuildRoad(silnice.get(1).getKonec(new Vector3(-20, 0, 0)),
                new Vector3(-20, -15, 0),
                selectedRoadProfile);

            // toto dela bordel
            //silnice.BuildRoad(silnice.get(1).getKonec(new Vector3(-20, 0, 0)),
            //    new Vector3(-20, -15, 0),
            //    selectedRoadProfile);

            silnice.BuildCross(
                silnice.get(6).getKonec(new Vector3(-20, -14, 0)),
                KrizovatkaType.T,
                2
            );

            silnice.BuildCross(
                silnice.get(7).getKonec(new Vector3(-38, -56, 0)),
                KrizovatkaType.T,
                0
                );

            silnice.BuildRoad(silnice.get(7).getKonec(new Vector3(-35, -30, 0)),
                new Vector3(-65, 0, 0),
                selectedRoadProfile);

            silnice.BuildRoad(
                new Vector3(-30, 60, 0),
                new Vector3(0, 90, 0),
                new Vector3(30, 60, 0),
                selectedRoadProfile
            );

            silnice.BuildRoad(silnice.get(10).getKonec(new Vector3(-30, 60, 0)),
                silnice.get(9).getKonec(new Vector3(-65, 0, 0)),
                selectedRoadProfile);

            silnice.get(4).deletable = silnice.get(5).deletable = silnice.get(7).deletable = silnice.get(11).deletable = silnice.get(12).deletable = false;

            // postavení staveb (zbourání bude stát peníze)
            silnice.BuildAll();
            silnice.AcceptCollisionTestBuffer();
            dijkstra.run = true;
            dijkstra.calculatePaths(dijkstra_debug);
            // tohle by chtelo parametrizovat a nacist z konfiguraku
            questLog.UpdatePassTime(paths.getPathCost(quest_path_start, quest_path_end));

            // pokus s vlajkou
            //foreach (var p in paths.getPaths()[quest_path_start][quest_path_end])
            //{
            //    flagList.Add(new Flag(FlagType.start), p.name);
            //}
            flagList.Add(new Flag(FlagType.start), "16");
            flagList.Add(new Flag(FlagType.end), "7");
            //flagList.Add(new Flag(FlagType.start), "7");
            //flagList.Add(new Flag(FlagType.end), "16");
            //flagList.Add(new Flag(FlagType.start), "11");
            //flagList.Add(new Flag(FlagType.start), "12");
            //flagList.Add(new Flag(FlagType.start), "13");
            //flagList.Add(new Flag(FlagType.start), "15");

            budovy = new BuildingKatalog(GraphicsDevice);
            budovy.AddModel(BuildingType.Standard, Content.Load<Model>("Content//Models//model_build"));
            aspectRatio = graphics.GraphicsDevice.Viewport.AspectRatio;
            budovy.Add(
                new Vector3(-3, 0, 0)
                );
            budovy.Add(
                new Vector3(20, 20, 0)
                );
            budovy.Add(
                new Vector3(35, 20, 0)
                );
            budovy.Add(
                new Vector3(50, 20, 0)
                );
    
            terrain = new TerrainGenerator(GraphicsDevice, terrainTexture, new Vector2(-300, -300), new Vector2(300, 300), 100);

            //song = Content.Load<Song>("Content//Sounds//transport_theme");
            //MediaPlayer.Play(song);
            //MediaPlayer.IsRepeating = true;

            shader = Content.Load<Effect>("Content\\Shader");
            shader.CurrentTechnique = shader.Techniques["diffuseColor"];
            shader.Parameters["World"].SetValue(Matrix.CreateRotationX(0));
            shader.Parameters["View"].SetValue(camera.viewMatrix);
            shader.Parameters["Projection"].SetValue(Matrix.CreatePerspectiveFieldOfView(
                MathHelper.ToRadians(viewAngle),
                (float)GraphicsDevice.Viewport.Width /
                (float)GraphicsDevice.Viewport.Height,
                1.0f, 1000.0f));
			base.LoadContent();

		}
		#endregion

		#region Unload Content
		/// <summary>
		/// Unloads content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here.
			base.UnloadContent();
		}
		#endregion

		#region Update
		/// <summary>
		/// Allows the application to run logic.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
            #region RayTracing
            string coord_str = "";
            int iid = 0;

            Utils.MouseController.Update(Mouse.GetState());

            mouseState = Mouse.GetState();
            mousePixel = new Rectangle(mouseState.X, mouseState.Y, 1, 1);

            Color[] pickedColor = new Color[1];
            bullet.Center = Utils.MouseController.Position3D(ref camera);



            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                // pokud je mys v herni obrazovce
                if (mouseState.X >= 0 && mouseState.X < window_width
                    && mouseState.Y >= 0 && mouseState.Y < window_height)
                {
                    // tohle by chtelo vyresit lepe, ale budiz...
                    if (mousePixel.X >= 1024) mousePixel.X = 1023;
                    backRT.GetData<Color>(0, mousePixel, pickedColor, 0, 1);

                    iid = pickedColor[0].R * 65536 + pickedColor[0].G * 255 + pickedColor[0].B;

                    coord_str = "Coordinates: [" + mouseState.X + ", " + mouseState.Y + "] - Color: [" + pickedColor[0].ToString() + "] id: " + iid;

                }
                else
                {
                    coord_str = config["Window"]["title"];
                }
            }
            else
            {
                coord_str = config["Window"]["title"];
            }
            Window.Title = coord_str;

            #endregion


            // pozice kurzoru pro zjisteni kliknuti na tlacitko v panelu nastroju 
            Rectangle pos = new Rectangle(mouseState.X, mouseState.Y, 1, 1);

            if (manager.FocusedControl == null)
            {
                bool toolChangePerformed = false;

                #region Tlacitka
                foreach (TButton b in button_list)
                {
                    if (pos.Intersects(b.rectangle))
                    {
                        if (mouseStatePrevious.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
                        { // stav tlacitka
                            toolChangePerformed = true;
                            silnice.activeTool = GUI.Tools.None;
                            if (b.id == 0)                        // Exit 
                            {
                                System.Environment.Exit(0);
                            }
                            else if (b.id == 1) { }               // Save 
                            else if (b.id == 2) { }               // Load 
                            else if (b.id == (int)GUI.Tools.EraseTool || b.id == (int)GUI.Tools.CrossBuilder || b.id == (int)GUI.Tools.RoadBuilder) // (3)Eraser (4)Cross (5)Curve 
                            {
                                silnice.activeTool = (GUI.Tools)b.id;
                                if (this.activeTool.activedTool == b.id)
                                {
                                    this.activeTool.deactiveTool();
                                    silnice.activeTool = GUI.Tools.None;
                                }
                                else
                                {
                                    this.activeTool.setActiveTool(b.id);
                                }

                                BuildRoadTool.Reset();
                                BuildCrossTool.Reset();
                            }
                            else if (b.id == 6)
                            {
                                WQuest.Show();
                            }               // Quest button
                            else if (b.id == 7) { 
                                silnice.showRoadLoad = !silnice.showRoadLoad;
                            }          // Settings
                        }
                        else
                        {
                            b.state = BState.Hover;
                        }
                    }
                    else
                    {
                        b.state = BState.None;
                    }
                }

                this.keyStatePrevious = this.keyState;
                this.keyState = Keyboard.GetState();
                if (this.keyStatePrevious.IsKeyDown(Keys.Escape) && this.keyState.IsKeyUp(Keys.Escape))
                {
                    this.activeTool.deactiveTool();
                }
                #endregion

                #region Nastroje
                if (toolChangePerformed == false)
                {
                    #region Staveni silnic
                    /******************
             * Staveni silnic
             *****************/
                    if (this.activeTool.activedTool == (int)GUI.Tools.RoadBuilder)
                    {

                        // resetování nástroje pravým tlačítkem
                        if (mouseStatePrevious.RightButton == ButtonState.Pressed && mouseState.RightButton == ButtonState.Released)
                        {
                            BuildRoadTool.Reset();
                        }

                        // kliknutí levým tlačítkem myši
                        if (mouseStatePrevious.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released)
                        {
                            // pokud je mys v herni obrazovce  
                            // dulezite, jinak bude hra padat, kdyz se mys dostane mimo okno hry
                            if (mouseState.X >= 0 && mouseState.X < graphics.GraphicsDevice.Viewport.Width
                                    && mouseState.Y >= 0 && mouseState.Y < graphics.GraphicsDevice.Viewport.Height)
                            {
                                if (mousePixel.X >= 1024)
                                {
                                    mousePixel.X = 1023; //id TODO
                                }
                                backRT.GetData<Color>(0, mousePixel, pickedColor, 0, 1); //id
                                int PickedId = pickedColor[0].R * 65536 + pickedColor[0].G * 255 + pickedColor[0].B;

                                if (PickedId != 0)
                                {
                                    BuildRoadTool.Action(silnice.get(PickedId).getKonec(bullet.Center));
                                }
                                else
                                {
                                    BuildRoadTool.Action(GeometryHelper.VectorZ(bullet.Center, 0.05f));
                                }
                            }

                        }

                        // je připravena nějaká akce nástroje?
                        if (BuildRoadTool.IsFinal())
                        {
                            switch (BuildRoadTool.Stage)
                            {
                                case BuildRoadStage.STAVIT_VOLNOU:
                                    silnice.BuildRoad(
                                        BuildRoadTool.X,
                                        BuildRoadTool.Y,
                                        BuildRoadTool.Z,
                                        selectedRoadProfile
                                        );
                                    break;
                                case BuildRoadStage.STAVIT_SPOJENI:
                                    if (useSimpleRoadConnector)
                                    {
                                        silnice.BuildConnectionRoad(
                                            BuildRoadTool.A,
                                            BuildRoadTool.B,
                                            selectedRoadProfile
                                            );
                                    }
                                    else
                                    {
                                        silnice.BuildRoad(
                                            BuildRoadTool.A,
                                            BuildRoadTool.B,
                                            selectedRoadProfile
                                            );
                                    }
                                    silnice.build = true;
                                    break;
                                case BuildRoadStage.STAVIT_PRODLOUZENI:
                                    silnice.BuildRoad(
                                        BuildRoadTool.A,
                                        BuildRoadTool.X,
                                        selectedRoadProfile
                                        );
                                    break;

                            }
                            questLog.SpendMoney(silnice.get(IDGenerator.getLastID().id).getPrice());
                            BuildRoadTool.Reset();
                        }


                    }
                    #endregion

                    #region Staveni krizovatek
                    if (this.activeTool.activedTool == (int)GUI.Tools.CrossBuilder)
                    {
                        if (Utils.MouseController.Click(Utils.MouseButton.Right))
                        {
                            BuildCrossTool.Reset();
                        }

                        if (Utils.MouseController.Click(Utils.MouseButton.Left))
                        {
                            // pokud je mys v herni obrazovce  
                            // dulezite, jinak bude hra padat, kdyz se mys dostane mimo okno hry
                            if (mouseState.X >= 0 && mouseState.X < graphics.GraphicsDevice.Viewport.Width
                                    && mouseState.Y >= 0 && mouseState.Y < graphics.GraphicsDevice.Viewport.Height)
                            {
                                if (mousePixel.X >= 1024)
                                {
                                    mousePixel.X = 1023; //id TODO
                                }

                                backRT.GetData<Color>(0, mousePixel, pickedColor, 0, 1); //id
                                int PickedId = pickedColor[0].R * 65536 + pickedColor[0].G * 255 + pickedColor[0].B;

                                if (PickedId != 0)
                                {
                                    BuildCrossTool.Action(silnice.get(PickedId).getKonec(bullet.Center));
                                }
                                else
                                {
                                    BuildCrossTool.Action(GeometryHelper.VectorZ(bullet.Center, 0.05f));
                                }
                            }
                        }

                        if (BuildCrossTool.IsFinal())
                        {
                            switch (BuildCrossTool.Stage)
                            {
                                case BuildCrossStage.STAVIT_NAPOJENOU:
                                    float buildAngle = GeometryHelper.UnsignedAngleBetweenTwoV3(BuildCrossTool.A.getDirection(), BuildCrossTool.X - BuildCrossTool.A.getPosition());
                                    int buildRameno = (int)((buildAngle * 3) / MathHelper.Pi);
                                    silnice.BuildCross(BuildCrossTool.A, selectedCrossType, buildRameno);
                                    break;
                                default:
                                    break;
                            }

                            questLog.SpendMoney(silnice.get(IDGenerator.getLastID().id).getPrice());
                            BuildCrossTool.Reset();
                        }
                    }
                    #endregion

                    #region Buldozér
                    if (this.activeTool.activedTool == (int)GUI.Tools.EraseTool)
                    {

                        if (Utils.MouseController.Click(Utils.MouseButton.Left))
                        {
                            // pokud je mys v herni obrazovce  
                            // dulezite, jinak bude hra padat, kdyz se mys dostane mimo okno hry
                            if (mouseState.X >= 0 && mouseState.X < graphics.GraphicsDevice.Viewport.Width
                                    && mouseState.Y >= 0 && mouseState.Y < graphics.GraphicsDevice.Viewport.Height)
                            {
                                if (mousePixel.X >= 1024)
                                {
                                    mousePixel.X = 1023; //id TODO
                                }

                                backRT.GetData<Color>(0, mousePixel, pickedColor, 0, 1); //id
                                int PickedId = pickedColor[0].R * 65536 + pickedColor[0].G * 255 + pickedColor[0].B;

                                if (PickedId != 0)
                                {
                                    if (silnice.get(PickedId).built == true && silnice.get(PickedId).deletable == true)
                                    {
                                        questLog.SpendMoney(silnice.get(PickedId).getDestroyPrice());
                                    }
                                    silnice.DestroyRoad(PickedId);
                                }
                            }
                        }
                    }
                    #endregion
                }
                #endregion
                    

                #region Ovladani kamery
                /*******************
             * Ovládání kamery *
             *******************/
                /* zoom - mění se Z pozice kamery */
                if (!this.activeTool.isActived()) //zablokování levého a pravého tlačítka myši při používání nástrojů
                {
                    /* posun */
                    if (mouseState.RightButton == ButtonState.Pressed)
                    {
                        camera.MoveRight((mouseState.X - mouseStatePrevious.X) / (float)GraphicsDevice.Viewport.Width * camera.position.Z * ((inverzniPosun) ? 1 : -1));
                        camera.MoveDown((mouseStatePrevious.Y - mouseState.Y) / (float)GraphicsDevice.Viewport.Height * camera.position.Z * ((inverzniPosun) ? -1 : 1));
                        cameraUpdate();
                    }
                    /* rotace (kolem osy Z) */
                    if (mouseState.LeftButton == ButtonState.Pressed)
                    {
                        //START RayPicking
                        if (iid != 0)
                        {
                            Window.Title = coord_str + " " + silnice.get(iid).getKonec(bullet.Center).getPosition();
                        }
                        else
                        {
                            Window.Title = coord_str + " Ray info: [" + bullet.Center.X + ";" + bullet.Center.Y + ";" + bullet.Center.Z + "]";
                        }
                        //END RayPicking

                        // rotuje Z 
                        camera.Rotate(ECameraRotations.eRollClockwise, (mouseState.Y - mouseStatePrevious.Y) / (float)GraphicsDevice.Viewport.Height * ((inverzniPosun) ? 1f : -1f));
                        cameraUpdate();
                    }
                }
                /* sklon (úhel pohledu) */
                if (mouseState.MiddleButton == ButtonState.Pressed)
                {
                    // rotuje X a Y
                    camera.Rotate(ECameraRotations.ePitchUp, (mouseState.Y - mouseStatePrevious.Y) / (float)GraphicsDevice.Viewport.Height * ((inverzniPosun) ? 1f : -1f));
                    camera.Rotate(ECameraRotations.eYawLeft, (mouseState.X - mouseStatePrevious.X) / (float)GraphicsDevice.Viewport.Width * ((inverzniPosun) ? 1f : -1f));
                    cameraUpdate();
                }
                if (mouseState.ScrollWheelValue > mouseStatePrevious.ScrollWheelValue)
                {
                    // zoom-in
                    if (camera.position.Z > 1.05f)
                    {
                        camera.MoveForward(zoomStep);
                        cameraUpdate();
                    }
                }
                else if (mouseState.ScrollWheelValue < mouseStatePrevious.ScrollWheelValue)
                {
                    // zoom-out
                    camera.MoveBack(zoomStep);
                    cameraUpdate();
                }
                #endregion

               

            }

            mouseStatePrevious = mouseState;
            // zatim to necham tady, pak se uvidi
            shader.Parameters["View"].SetValue(camera.viewMatrix);
            WQuest.Update(questLog);
			base.Update(gameTime);
            manager.Update(gameTime);

		}
		#endregion

		#region Draw
		/// <summary>
		/// This is called when the application should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{

            drawToRT(collisionRT, RTarget.Collision, Color.Black);
            drawToRT(backRT, RTarget.Back, Color.Black);

            graphics.GraphicsDevice.SetRenderTarget(null);
            manager.BeginDraw(gameTime);

            spriteBatch.Begin();
            foreach (TButton b in button_list)
            {
                if (activeTool.activedTool == b.id)
                {
                    spriteBatch.Draw(b.texture_click, b.rectangle, Color.White);
                }
                else if (b.state == BState.Hover)
                {
                    spriteBatch.Draw(b.texture_hover, b.rectangle, Color.White);
                }
                else if (b.state == BState.Clicked)
                {
                    spriteBatch.Draw(b.texture_click, b.rectangle, Color.White);
                }
                else
                {
                    spriteBatch.Draw(b.texture, b.rectangle, Color.White);
                }
            }
            shader.Parameters["texturize"].SetValue(true);
            shader.Parameters["xTexture"].SetValue(terrainTexture);
            terrain.Draw(shader);
            silnice.Draw(shader, RTarget.Regular, roadTexture);
            budovy.Draw(shader, RTarget.Regular);
            if (WQuest.markPoints.Checked == true)
            {
                flagList.Draw(shader, RTarget.Regular);
            }
            spriteBatch.End();

            manager.EndDraw();

            if (silnice.collisionChecked == false)
            {
                if (CollisionDetection.PerPixel(collisionRT, window_width, window_height) == true)
                {
                    //questLog.SpendMoney(-silnice.get(IDGenerator.getLastID().id).getPrice());
                    //ModalWindow.ShowModal("Nelze postavit silnici!", "Silnice by se překrývaly.");
                    mw.ShowModal("Nelze postavit silnici!", "Silnice by se překrývaly.");
                    silnice.RejectCollisionTestBuffer();
                    //silnice.DestroyRoad(IDGenerator.getLastID().id, true);
                }
                else
                {
                    silnice.AcceptCollisionTestBuffer();
                }
                silnice.collisionChecked = true;
            }
		}

        void drawToRT(RenderTarget2D target, RTarget type, Color background)
        {
            window_width = graphics.GraphicsDevice.Viewport.Width;
            window_height = graphics.GraphicsDevice.Viewport.Height;

            graphics.GraphicsDevice.SetRenderTarget(target);
            //if(type != RTarget.Regular)
            graphics.GraphicsDevice.Clear(background);

            if (type == RTarget.Regular)
            {
                shader.Parameters["texturize"].SetValue(true);
                shader.Parameters["xTexture"].SetValue(terrainTexture);
                terrain.Draw(shader);
            }


            // vykresleni silnic
            if (type == RTarget.Regular)
            {
                silnice.Draw(shader, type, roadTexture);
            }
            else
                silnice.Draw(shader, type);



            //Bounding boxes
            if (type == RTarget.Regular)
            {

                budovy.Draw(shader, type);
            }
            else if (type == RTarget.Collision)
            {
                shader.Parameters["texturize"].SetValue(false);
                shader.Parameters["collision"].SetValue(true);
                shader.Parameters["collisionSprite"].SetValue(false);
                budovy.Draw(shader, type);
                shader.Parameters["collision"].SetValue(false);
            }
            //spriteBatch.End();
        }
		#endregion

        //polohovani kamery
        private void cameraUpdate()
        {
            shader.Parameters["View"].SetValue(camera.viewMatrix);
        }
	}
}
