﻿using System;
using Microsoft.Xna.Framework;

namespace transport
{
    enum BuildRoadStage
    {
        RESET,                  // počáteční stav
        PRVNI_BOD,              // kliknuto na bod
        DRUHY_BOD,              // kliknuto na druhý bod
        PRVNI_KONEC_SILNICE,    // kliknuto na první konec silnice
        STAVIT_VOLNOU,          // kliknuto na třetí bod - stavit volnou silnici
        STAVIT_PRODLOUZENI,     // kliknuto na konec silnice a bod - prodloužit silnici
        STAVIT_SPOJENI,         // kliknuto na dva konce silnice
        FAIL,                   // stavila se volna, ale kliklo se na konec silnice misto na bod
    }

    enum BuildCrossStage
    {
        RESET,                  // počáteční stav
        PRVNI_BOD,              // kliknuto na bod
        PRVNI_KONEC_SILNICE,    // kliknuto na konec silnice
        STAVIT_VOLNOU,          
        STAVIT_NAPOJENOU,     
        FAIL,                  
    }

    static class BuildCrossTool
    {
        public static Vector3 X, Y;
        public static KonecSilnice A;

        public static BuildCrossStage Stage;

        public static void Reset()
        {
            Stage = BuildCrossStage.RESET;
        }

        public static bool IsFinal()
        {
            switch (Stage)
            {
                case BuildCrossStage.STAVIT_NAPOJENOU:
                case BuildCrossStage.STAVIT_VOLNOU:
                    return true;
                default:
                    return false;
            }
        }

        public static void Action(KonecSilnice K){
            switch(Stage){
                case BuildCrossStage.PRVNI_BOD:
                    A = K;
                    Stage = BuildCrossStage.STAVIT_NAPOJENOU;
                    break;
                case BuildCrossStage.PRVNI_KONEC_SILNICE:
                    Stage = BuildCrossStage.FAIL;
                    break;
                case BuildCrossStage.RESET:
                    A = K;
                    Stage = BuildCrossStage.PRVNI_KONEC_SILNICE;
                    break;
                default:
                    break;
            }

            if (Stage == BuildCrossStage.FAIL)
            {
                Reset();
            }
        }

        public static void Action(Vector3 P)
        {
            switch (Stage)
            {
                case BuildCrossStage.PRVNI_BOD:
                    Y = P;
                    Stage = BuildCrossStage.FAIL; // volná křižovatka není implementována
                    break;
                case BuildCrossStage.PRVNI_KONEC_SILNICE:
                    X = P;
                    Stage = BuildCrossStage.STAVIT_NAPOJENOU;
                    break;
                case BuildCrossStage.RESET:
                    X = P;
                    Stage = BuildCrossStage.PRVNI_BOD;
                    break;
                default:
                    break;
            }

            if (Stage == BuildCrossStage.FAIL)
            {
                Reset();
            }
        }

        
    }

    /// <summary>
    /// Nástroj pro stavění silnic na bázi automatu.
    /// </summary>
    static class BuildRoadTool
    {
        public static KonecSilnice A, B;
        public static Vector3 X, Y, Z;

        public static BuildRoadStage Stage;

        public static void Reset()
        {
            Stage = BuildRoadStage.RESET;
        }

        /// <summary>
        /// Je v konečném stavu?
        /// </summary>
        /// <returns></returns>
        public static bool IsFinal()
        {
            switch (Stage)
            {
                case BuildRoadStage.STAVIT_PRODLOUZENI:
                case BuildRoadStage.STAVIT_SPOJENI:
                case BuildRoadStage.STAVIT_VOLNOU:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Provede akci "kliknuto na konec silnice"
        /// </summary>
        /// <param name="K"></param>
        public static void Action(KonecSilnice K)
        {
            switch (Stage)
            {
                case BuildRoadStage.RESET:
                    A = K;
                    Stage = BuildRoadStage.PRVNI_KONEC_SILNICE;
                    break;
                case BuildRoadStage.PRVNI_KONEC_SILNICE:
                    B = K;
                    Stage = BuildRoadStage.STAVIT_SPOJENI;
                    break;
                case BuildRoadStage.PRVNI_BOD:
                    A = K;
                    Stage = BuildRoadStage.STAVIT_PRODLOUZENI;
                    break;
                case BuildRoadStage.DRUHY_BOD:
                    Stage = BuildRoadStage.FAIL;
                    break;
                default:
                    break;
            }

            if (Stage == BuildRoadStage.FAIL)
            {
                Stage = BuildRoadStage.RESET;
            }
        }

        /// <summary>
        /// Provede akci "kliknuto na bod"
        /// </summary>
        /// <param name="P"></param>
        public static void Action(Vector3 P)
        {
            switch (Stage)
            {
                case BuildRoadStage.RESET:
                    X = P;
                    Stage = BuildRoadStage.PRVNI_BOD;
                    break;
                case BuildRoadStage.PRVNI_KONEC_SILNICE:
                    X = P;
                    Stage = BuildRoadStage.STAVIT_PRODLOUZENI;
                    break;
                case BuildRoadStage.PRVNI_BOD:
                    Y = P;
                    Stage = BuildRoadStage.DRUHY_BOD;
                    break;
                case BuildRoadStage.DRUHY_BOD:
                    Z = P;
                    Stage = BuildRoadStage.STAVIT_VOLNOU;
                    break;
                default:
                    break;
            }

        }
    }

    class ActiveTool
    {
        public bool realtimeNahled = false;
        public int activedTool = -1;
        public int stage = 0;
        public int objectId = 0;
        public Vector3[] points = { new Vector3(0, 0, 0), new Vector3(0, 0, 0), new Vector3(0, 0, 0) };
        public KonecSilnice[] RoadEnd = { new KonecSilnice(new Vector3(0, 0, 0), new Vector3(0, 0, 0)), new KonecSilnice(new Vector3(0, 0, 0), new Vector3(0, 0, 0)) };

        public ActiveTool(int tool = -1)
        {
            this.activedTool = tool;
        }

        public void setActiveTool(int tool)
        {
            this.resetTool();
            this.activedTool = tool;
        }

        public bool isActived()
        {
            if (this.activedTool == -1) return false;
            return true;
        }

        public int getActiveTool()
        {
            return this.activedTool;
        }

        public void resetTool()
        {
            this.realtimeNahled = false;
            this.stage = 0;
            this.objectId = 0;
            for (int i = 0; i < 3; i++)
            {
                this.points[i].X = 0;
                this.points[i].Y = 0;
                this.points[i].Z = 0;
            }
            for (int i = 0; i < 2; i++)
            {
                this.RoadEnd[i].reset();
            }
        }
        public void deactiveTool()
        {
            this.activedTool = -1;
            this.resetTool();
        }
        public Vector3 stredOpsaneKruznice()
        {
            return (new Vector3(
                ((-this.points[1].Y * this.points[2].X * this.points[2].X + this.points[0].X * this.points[0].X * (this.points[1].Y - this.points[2].Y) + this.points[0].X * this.points[0].X * (this.points[1].Y - this.points[2].Y) + this.points[1].X * this.points[1].X * this.points[2].Y + this.points[1].Y * this.points[1].Y * this.points[2].Y - this.points[1].Y * this.points[2].Y * this.points[2].Y + this.points[0].X * (-this.points[1].X * this.points[1].X - this.points[1].Y * this.points[1].Y + this.points[2].X * this.points[2].X + this.points[2].Y * this.points[2].Y)) /
                    (2 * (-this.points[1].Y * this.points[2].X + this.points[0].X * (-this.points[1].X + this.points[2].X) + this.points[0].X * (this.points[1].Y - this.points[2].Y) + this.points[1].X * this.points[2].Y))),
                 (-this.points[1].X * this.points[1].X * this.points[2].X - this.points[1].Y * this.points[1].Y * this.points[2].X + this.points[1].X * this.points[2].X * this.points[2].X + this.points[0].X * this.points[0].X * (-this.points[1].X + this.points[2].X) + this.points[0].X * this.points[0].X * (-this.points[1].X + this.points[2].X) + this.points[1].X * this.points[2].Y * this.points[2].Y + this.points[0].X * (this.points[1].X * this.points[1].X + this.points[1].Y * this.points[1].Y - this.points[2].X * this.points[2].X - this.points[2].Y * this.points[2].Y)) /
                    (2 * (-this.points[1].Y * this.points[2].X + this.points[0].X * (-this.points[1].X + this.points[2].X) + this.points[0].X * (this.points[1].Y - this.points[2].Y) + this.points[1].X * this.points[2].Y)),
                 0));
        }
        public Vector3 stredOpsaneKruznice(Vector3[] threePoints)
        {
            return (new Vector3(
                ((-threePoints[1].Y * threePoints[2].X * threePoints[2].X + threePoints[0].X * threePoints[0].X * (threePoints[1].Y - threePoints[2].Y) + threePoints[0].X * threePoints[0].X * (threePoints[1].Y - threePoints[2].Y) + threePoints[1].X * threePoints[1].X * threePoints[2].Y + threePoints[1].Y * threePoints[1].Y * threePoints[2].Y - threePoints[1].Y * threePoints[2].Y * threePoints[2].Y + threePoints[0].X * (-threePoints[1].X * threePoints[1].X - threePoints[1].Y * threePoints[1].Y + threePoints[2].X * threePoints[2].X + threePoints[2].Y * threePoints[2].Y)) /
                    (2 * (-threePoints[1].Y * threePoints[2].X + threePoints[0].X * (-threePoints[1].X + threePoints[2].X) + threePoints[0].X * (threePoints[1].Y - threePoints[2].Y) + threePoints[1].X * threePoints[2].Y))),
                 (-threePoints[1].X * threePoints[1].X * threePoints[2].X - threePoints[1].Y * threePoints[1].Y * threePoints[2].X + threePoints[1].X * threePoints[2].X * threePoints[2].X + threePoints[0].X * threePoints[0].X * (-threePoints[1].X + threePoints[2].X) + threePoints[0].X * threePoints[0].X * (-threePoints[1].X + threePoints[2].X) + threePoints[1].X * threePoints[2].Y * threePoints[2].Y + threePoints[0].X * (threePoints[1].X * threePoints[1].X + threePoints[1].Y * threePoints[1].Y - threePoints[2].X * threePoints[2].X - threePoints[2].Y * threePoints[2].Y)) /
                    (2 * (-threePoints[1].Y * threePoints[2].X + threePoints[0].X * (-threePoints[1].X + threePoints[2].X) + threePoints[0].X * (threePoints[1].Y - threePoints[2].Y) + threePoints[1].X * threePoints[2].Y)),
                 0));
        }
        public Vector3 stredOpsaneKruznice(Vector3 A, Vector3 B, Vector3 C)
        {
            return (new Vector3(
                ((-B.Y * C.X * C.X + A.X * A.X * (B.Y - C.Y) + A.X * A.X * (B.Y - C.Y) + B.X * B.X * C.Y + B.Y * B.Y * C.Y - B.Y * C.Y * C.Y + A.X * (-B.X * B.X - B.Y * B.Y + C.X * C.X + C.Y * C.Y)) /
                    (2 * (-B.Y * C.X + A.X * (-B.X + C.X) + A.X * (B.Y - C.Y) + B.X * C.Y))),
                 (-B.X * B.X * C.X - B.Y * B.Y * C.X + B.X * C.X * C.X + A.X * A.X * (-B.X + C.X) + A.X * A.X * (-B.X + C.X) + B.X * C.Y * C.Y + A.X * (B.X * B.X + B.Y * B.Y - C.X * C.X - C.Y * C.Y)) /
                    (2 * (-B.Y * C.X + A.X * (-B.X + C.X) + A.X * (B.Y - C.Y) + B.X * C.Y)),
                 0));
        }

        public float vzdalenost(Vector2 A, Vector2 B)
        {
            return (float) Math.Sqrt((A.X - B.X) * (A.X - B.X) + (A.Y - B.Y) * (A.Y - B.Y));
        }
        public float vzdalenost(Vector3 A, Vector3 B)
        {
            return (float)Math.Sqrt((A.X - B.X) * (A.X - B.X) + (A.Y - B.Y) * (A.Y - B.Y) + (A.Z - B.Z) * (A.Z - B.Z));
        }
        public float vzdalenost(int A, int B)
        {
            if (naleziIntervalu(A, 0, 2) && naleziIntervalu(B, 0, 2))
                return (float)Math.Sqrt((this.points[A].X - this.points[B].X) * (this.points[A].X - this.points[B].X) +
                    (this.points[A].Y - this.points[B].Y) * (this.points[A].Y - this.points[B].Y) +
                    (this.points[A].Z - this.points[B].Z) * (this.points[A].Z - this.points[B].Z));
            else return 0;
        }
        private bool naleziIntervalu(int X, int min, int max)
        {
            if (X < min) return false;
            if (X > max) return false;
            return true;
        }
    }
}
