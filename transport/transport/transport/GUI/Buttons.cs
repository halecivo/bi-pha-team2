
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace transport
{
	public enum BState {
			None,
			Hover,
			Clicked
	};

    public class TButton
    {
		public Texture2D texture;
		public Texture2D texture_hover;
		public Texture2D texture_click;

        public string texture_file;
        public string texture_file_hover;
        public string texture_file_click;
        
		public Rectangle rectangle;
        public BState state;

		public int id;

        public TButton (int id, string texture, string texture_hover, string texture_click, int xpos, int ypos, int size = 64)
        {
			this.id = id;

			this.texture_file = texture;
			this.texture_file_hover = texture_hover;
			this.texture_file_click = texture_click;
            
			this.rectangle = new Rectangle(xpos, ypos, size, size);
            this.state = BState.None;
        }
    }
}