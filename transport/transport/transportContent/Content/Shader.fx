float4x4 World;
float4x4 View;
float4x4 Projection;

float3 pickColor;
bool texturize;
bool clicked;
bool locked;
bool isOnBestPath;
bool collision;
bool collisionSprite;
int car_count;

Texture xTexture;
sampler TextureSampler = sampler_state {
	texture = <xTexture>;
	magfilter = LINEAR;
	minfilter = LINEAR;
	mipfilter = LINEAR;
	AddressU = mirror;
	AddressV = mirror;
};

struct VertexShaderData
{
    float4 Position : POSITION0;
	float2 TextureCoord: TEXCOORD0;
};

struct PixelShaderData
{
    float4 regular : COLOR0;   
    //float4 back : COLOR1;   
};

VertexShaderData VertexShaderFunction(float4 inPos : POSITION, float2 texCoord: TEXCOORD0)
{
    VertexShaderData output;

    float4 worldPosition = mul(inPos, World);
    float4 viewPosition = mul(worldPosition, View);
    
	output.Position = mul(viewPosition, Projection);
	output.TextureCoord = texCoord;

    return output;
}

PixelShaderData PixelShaderFunction(VertexShaderData input)
{
    PixelShaderData output;
    
	if (texturize) {
		output.regular = tex2D(TextureSampler, input.TextureCoord);
		
		if (locked) {
			output.regular.r += 0.3;
		}
		if (isOnBestPath){
			output.regular.g += 0.2;
		}

		// zelena
		if (car_count > 0 && car_count < 25) {
			output.regular.g += 0.4;	
		}
		else if (car_count >= 25 && car_count < 50) {
			output.regular.r += 0.1;
			output.regular.g += 0.4;
		}
		else if (car_count >= 50 && car_count < 75) {
			output.regular.r += 0.2;
			output.regular.g += 0.4;
		}
		else if (car_count >= 75 && car_count < 100) {
			output.regular.r += 0.3;
			output.regular.g += 0.4;
		}
		// zluta
		else if (car_count >= 100 && car_count < 125) {
			output.regular.r += 0.4;
			output.regular.g += 0.4;
		}
		else if (car_count >= 125 && car_count < 150) {
			output.regular.r += 0.4;
			output.regular.g += 0.3;
		}
		else if (car_count >= 150 && car_count < 175) {
			output.regular.r += 0.4;
			output.regular.g += 0.2;
		}
		else if (car_count >= 175 && car_count < 200) {
			output.regular.r += 0.4;
			output.regular.g += 0.1;
		}
		//cervena
		else if (car_count >= 200) {
			output.regular.r += 0.4;
		}
	}
	else if(collision){
		if(collisionSprite){
			output.regular.rgb = float3(0, 0, 255);
		}
		else{
			output.regular.rgb = float3(0, 255, 0);
		}
		output.regular.a = 0.5;
	}
	else {
		output.regular.rgb = pickColor;
		//debug
		//output.regular.rgb = float3(0,255,255);
		output.regular.a = 255;
	}
  
    return output;
}

technique diffuseColor
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
